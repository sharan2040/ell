<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staticpages extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		$this->load->helper('url');
		$this->load->model('Enquiry_model');
		$this->load->model('Settings_model');
		$this->load->model('Blog_model');
	}

	public function index()
	{
		//Get Data from Model
		$data['blogs'] =  $this->Blog_model->listBlogs(FALSE, FALSE);
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Company Tag';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);
		// print_r($footerData); break;

		$this->load->view('/themes/header', $headerData);
		$this->load->view('home', $data);
		$this->load->view('/themes/footer', $footerData);	
	}

	public function page404()
	{
		$this->load->view('404');
	}

	public function careers()
	{
		$this->load->model('Career_model');
		$data['careers'] = $this->Career_model->listCareers(FALSE, array('application_enddate >=' => date('Y-m-d') ));
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Careers';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('careers', $data);
		$this->load->view('/themes/footer', $footerData);	
	}

	public function contactUs()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Contact us';
		$footerData['settings'] = $websiteSettings;
		$data['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('contact', $data);
		$this->load->view('/themes/footer', $footerData);	
	}

	public function contactSendMail(){

		$to_email = $this->input->post('email'); 

         //Load email library 
		$this->load->library('email'); 

		$config['protocol'] = "mail";
		$config['smtp_host'] = "mail.elegantlinelogistics.com";
		$config['smtp_port'] = "25";
		$config['smtp_user'] = "info@elegantlinelogistics.com"; 
		$config['smtp_pass'] = "55ELLinfo";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$this->email->initialize($config);

		$this->email->from('info@elegantlinelogistics.com', 'Elegant Line Logistics');
		$this->email->to($to_email);
		$this->email->subject('Thanks for contacting us');
		$this->email->message('Thanks for contacting us!');

		//Send mail 
		if($this->email->send()) 
			echo "Email sent successfully."; 
		else 
			echo "Error in sending Email.";
	}

	public function localRemoval()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Local Removals';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('localRemoval');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function internationalRemoval()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'International Removals';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('internationalRemoval');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function officeRemoval()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Office Relocations';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('officeRemoval');
		$this->load->view('/themes/footer', $footerData);	
	}	


	public function industrialPacking()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Industrial Packing';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('industrialPacking');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function cargoLashingServices()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Cargo Lashing';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('cargoLashingServices');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function freightForwarding()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Freight Forwarding';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('freightforwarding');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function newsEvents()
	{
		$this->load->model('Blog_model');
		$data['blogs'] =  $this->Blog_model->listBlogs(FALSE, FALSE);

		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'News & Events';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('newsEvents', $data);
		$this->load->view('/themes/footer', $footerData);	
	}

	public function newsDetails($slug = false)
	{
		$this->load->model('Blog_model');
		if($slug!= false){
			$data['blogDetails'] =  $this->Blog_model->listBlogs(FALSE, array('slug' => $slug));
			$data['allBlogs'] =  $this->Blog_model->listBlogs(array('blog_title','slug'), FALSE, 10);

			$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
			$headerData['settings'] = $websiteSettings;
			$headerData['title'] = $data['blogDetails'][0]->blog_title;
			$footerData['settings'] = $websiteSettings;
			$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

			//Increment View Count by 1
			$this->Blog_model->incrementView($slug);

			$this->load->view('/themes/header', $headerData);
			$this->load->view('newsDetails', $data);
			$this->load->view('/themes/footer', $footerData);	

		} else {
			redirect('/news');
		}

	}

	public function requestQuote()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Request a Quote';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('requestQuote');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function faq()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Frequently Asked Questions';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);
		$this->load->model('Faq_model');
		$data['faqs'] =  $this->Faq_model->listFaq(FALSE, FALSE);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('faq', $data);
		$this->load->view('/themes/footer', $footerData);	
	}	

	public function storage()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Storage & Warehousing';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('storage');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function vehicleRelocation()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Vehicle Relocation';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('vehicleRelocation');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function aboutUs()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'About us';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('aboutUs');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function termsConditions()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Terms & Conditions';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('termsConditions');
		$this->load->view('/themes/footer', $footerData);	
	}

	public function trackYourShipment()
	{
		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);
		$headerData['settings'] = $websiteSettings;
		$headerData['title'] = 'Track your Shipment';
		$footerData['settings'] = $websiteSettings;
		$footerData['recentBlogs'] =  $this->Blog_model->listBlogs( array('blog_title', 'slug', 'contents', 'views'), FALSE, 2);

		$this->load->view('/themes/header', $headerData);
		$this->load->view('trackYourShipment');
		$this->load->view('/themes/footer', $footerData);	
	}


	//Fetch Details of a project/shipment
	//AJAX Call
	public function fetchDetails()
	{	
		$uniqueId = file_get_contents('php://input'); 
		$this->load->model('Projects_model');	
		$project =  $this->Projects_model->listProjects(FALSE, array('unique_id' => $uniqueId ));

		if(!empty($project)){
			echo $project[0]->current_status;
		} else {
			echo "No Description Available. Contact us if you think its is an error at our side";
		}

	}

	public function addQuote(){

		$data = [
		'client_name' => $this->input->post('clientName'),
		'type_of_project' => $this->input->post('typeOfProject'),
		'client_email' => $this->input->post('clientEmail'),
		'client_company' => $this->input->post('clientCompany'),
		'phone_number' => $this->input->post('phoneNumber'),
		'date_of_move' => $this->input->post('dateOfMove'),
		'zip_code' => $this->input->post('zipCode'),
		'square_footage' => $this->input->post('squareFootage'),
		'description' => $this->input->post('description'),
		];

		$this->Enquiry_model->createEnquiry($data);

		$to_email = $this->input->post('clientEmail');

         //Load email library 
		$this->load->model('Email_model');
		$mailTemplate = $this->Email_model->listTemplates( FALSE, array('type' => 'welcome' ) );

		$websiteSettings =  $this->Settings_model->getSettings(FALSE, FALSE);


		$variablesToReplace = array (
		    '{{fullname}}' => $data['client_name'],
		    '{{address}}' => $websiteSettings[0]->company_address,
		    '{{fax}}' => $websiteSettings[0]->contact_fax,
		    '{{phone}}' => $websiteSettings[0]->contact_phone,
		    '{{imageurl}}' => base_url()."assets/mail_header.jpg"
		);

		$message = str_replace(array_keys($variablesToReplace), $variablesToReplace, $mailTemplate[0]->template);
		echo $to_email;

		$this->load->library('email');
		$config['protocol'] = "mail";
		$config['smtp_host'] = "mail.gmail.com";
		$config['smtp_port'] = "25";
		$config['smtp_user'] = "technical@elegantlinelogistics.com"; 
		$config['smtp_pass'] = "Mohandaskm55";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$this->email->initialize($config);

		$this->email->from('technical@elegantlinelogistics.com', 'Elegant Line Logistics');
		$this->email->to($to_email);
		$this->email->subject('Recieved your Quote Request');
		$this->email->message($message);

		//Send mail 
		if($this->email->send()) 
			echo "Email sent successfully."; 
		else 
			echo "Error in sending Email.";


	}

	public function sendMail(){
		$this->load->model('Email_model');
		$mailTemplate = $this->Email_model->listTemplates( FALSE, array('type' => 'welcome' ) );

		$to_email = 'sharan2040@gmail.com'; 

		$this->load->library('email');
		$config['protocol'] = "mail";
		$config['smtp_host'] = "mail.gmail.com";
		$config['smtp_port'] = "25";
		$config['smtp_user'] = "technical@elegantlinelogistics.com"; 
		$config['smtp_pass'] = "Mohandaskm55";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$this->email->initialize($config);

		$this->email->from('technical@elegantlinelogistics.com', 'Elegant Line Logistics');
		$this->email->to($to_email);
		$this->email->subject('Recieved your Quote Request');
		$this->email->message($mailTemplate[0]->template);
		$this->email->send();
	}
} // Controller Ends Here
