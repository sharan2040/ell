<?php

class Auth extends CI_Controller {

    public function __construct() {
        
        parent::__construct();

        $this->load->model('user_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('url');
    }

    /**
     * @author  	Sharan
     * @uses    	Login Function
     */
    public function login() {

        try {

            if($this->session->userdata('user_id')){
              redirect('admin/dashboard');
            }

            //Form Validation
            $this->form_validation->set_rules('email', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/login');
            } else {

                $conditions = array(
                    'email' => $this->input->post('email'),
                    'password_hash'=> $this->input->post('password'),
                    'status' => '1'
                );

                $userDetails = $this->user_model->userData(FALSE, $conditions);

                if ($userDetails) {
                    $data['loginSuccess'] = TRUE;
                    $data['message'] = "Welcome back";
                    $this->session->set_flashdata($data); 

                    //Create Session 
                    $sessionData = [
                        'full_name' => $userDetails[0]->full_name,
                        'user_id' => $userDetails[0]->user_id,
                        'email' => $userDetails[0]->email,
                    ];
                    $this->session->set_userdata($sessionData);
                    redirect('admin/dashboard');
                }
                else {
                        $data['loginFailed'] = TRUE;
                        $data['message'] = "Invalid login credentials! Please try again";
                        $this->session->set_flashdata($data);
                        redirect('admin/login');
                }
            }
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    /**
     * @author  	Sharan
     * @uses    	Logout Function
     */
    public function logout() {

        try {
            $this->session->sess_destroy();        
            redirect('admin');
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }


} // Controller Ends Here