<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Blog extends Admin_Controller{
	
	public function __construct(){
		parent:: __construct();

		$this->load->model('Blog_model');
		$this->load->Model('Reminder_model');
	}

	public function index(){
		$data['blogs'] = $this->Blog_model->listBlogs(FALSE, FALSE);
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/blog/list' ,$data );
	}

	public function add(){
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/blog/add');
	}

	public function create(){

		 //Image Upload Starts
		$config['upload_path'] = FCPATH . 'assets/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name'] = md5(uniqid(rand(), true));
		$config['overwrite'] = false;

        // print_r($config['upload_path']);
		$this->load->library('upload', $config);


		if ( ! $this->upload->do_upload('coverImage'))
		{
			$error = array('error' => $this->upload->display_errors());
			print_r($error); break;
			$uploadedImage = '';
		}
		else
		{
			$uploadedImage = $this->upload->data();
		}


        //Image Upload Ends
		$data = [
		'blog_title' => $this->input->post('blogTitle'),
		'slug' => url_title( $this->input->post('blogTitle') , 'dash', true),
		'contents' => $this->input->post('contents'),
		];

		if(isset($uploadedImage['file_name'])){
			$data['cover_image'] = $uploadedImage['file_name'];
		}

		//For Displaying message in the next page
		$this->session->set_flashdata('blogCreated', true);

		$this->Blog_model->createBlog($data);
		redirect('admin/blog');
	}

	public function edit($blogId){
		$data['blogs'] = $this->Blog_model->listBlogs( FALSE, array('blog_id' => $blogId) );
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/blog/edit', $data);
	}

	public function update(){

		$blogId = $this->input->post('blogId', TRUE);

		 //Image Upload Starts
		$config['upload_path'] = FCPATH . 'assets/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name'] = md5(uniqid(rand(), true));
		$config['overwrite'] = false;

        // print_r($config['upload_path']);
		$this->load->library('upload', $config);

		if($this->input->post('coverImage')){
			if ( ! $this->upload->do_upload('coverImage'))
			{
				$error = array('error' => $this->upload->display_errors());
				print_r($error); break;
				$uploadedImage = '';
			}
			else
			{
				$uploadedImage = $this->upload->data();
			}
		}


        //Image Upload Ends
		$data = [
		'blog_title' => $this->input->post('blogTitle'),
		'slug' => url_title( $this->input->post('blogTitle') , 'dash', true),
		'contents' => $this->input->post('contents', TRUE),
		];

		if(isset($uploadedImage['file_name'])){
			$data['cover_image'] = $uploadedImage['file_name'];
		}

		$this->Blog_model->updateBlog( $data, array('blog_id' => $blogId ) );

		//For Displaying message in the next page
		$this->session->set_flashdata('blogUpdated', true);

		redirect('admin/blog');
	}

	public function delete(){

		$blogId = $this->input->post('blogId', TRUE);

		if($blogId){
			$this->Blog_model->deleteBlog( array('blog_id' => $blogId ) );

		//For Displaying message in the next page
			$this->session->set_flashdata('blogDeleted', true);

			redirect('admin/blog');
		}
		else{
			redirect('admin/blog');
		}
	}

}// Controller ends here



