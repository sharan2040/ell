<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careers extends Admin_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('Career_model');
		$this->load->Model('Reminder_model');

	}


    /**
     * @author  Sharan Mohandas
     * @uses    To Insert/Update Offer | Adds offer amount to Chat
     * @param   $data [array] - Offer Data
     * @return  boolean/insert_id
    */
	public function index()
	{
		$data['careers'] = $this->Career_model->listCareers(FALSE, FALSE);
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/careers/view', $data);
		// $this->load->view('/themes/admin-footer');		
	}

	 /**
     * @author  Sharan Mohandas
     * @uses    To Insert/Update Offer | Adds offer amount to Chat
     * @param   $data [array] - Offer Data
     * @return  boolean/insert_id
    */
	public function create()
	{

		$data = [
			'job_title' => $this->input->post('jobTitle', TRUE),
			'job_location' => $this->input->post('jobLocation', TRUE),
			'job_description' => $this->input->post('jobDescription', TRUE),
			'application_enddate' => date("Y-m-d", strtotime( $this->input->post('endDate', TRUE) ) ),
		];

		$this->Career_model->createCareer( $data );

		//For Displaying message in the next page
		$this->session->set_flashdata('careerCreated', true);

		redirect('admin/careers');
	}

	public function edit($careerId){
		$data['careers'] = $this->Career_model->listCareers(false, array('career_id' => $careerId));
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/careers/edit', $data);
	}
  
	public function updateCareer(){

		$careerId = $this->input->post('careerId', TRUE);

		$data = [
			'job_title' => $this->input->post('jobTitle', TRUE),
			'job_location' => $this->input->post('jobLocation', TRUE),
			'job_description' => $this->input->post('jobDescription', TRUE),
			'application_enddate' => date("Y-m-d", strtotime( $this->input->post('applicationEndDate', TRUE) ) ),
		];

		$this->Career_model->updatecareer( $data, array('career_id' => $careerId) );

		//For Displaying message in the next page
		$this->session->set_flashdata('careerUpdated', true);

		redirect('admin/careers');
	}	

	public function deleteCareer(){
		$careerId = $this->input->post('careerId', TRUE);
		// print_r($careerId); break;
		if($careerId){
			$this->Career_model->deleteCareer( array('career_id' => $careerId ) );

			//For Displaying message in the next page
			$this->session->set_flashdata('careerDeleted', true);

			redirect('admin/careers');
		}
		else{
			redirect('admin/careers');
		}
		
	}

} // Controller Ends Here
