<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	
	public function index()
	{
		$this->load->model('Enquiry_model');
		$this->load->model('Reminder_model');

		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		
		$this->load->view('admin/dashboard');
		$this->load->view('/themes/admin-footer');	
	}

	

} // Controller Ends Here
