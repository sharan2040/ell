<?php  
defined('BASEPATH') OR exit('No direct script access allowed');
 

/**
* 
*/
class Enquiries extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->Model('Enquiry_model');
		$this->load->Model('Reminder_model');

	}

	public function index(){

		//Fetch data from Model
		$data['enquiries'] = $this->Enquiry_model->listEnquiries(false ,false);
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/enquiry/list', $data);
		$this->load->view('/themes/admin-footer');

	}

	public function viewEnquiries($enquiryId){

		$data['enquiries'] = $this->Enquiry_model->listEnquiries(false, array('enquiry_Id' => $enquiryId ) );
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
		
		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/enquiry/view', $data);
		$this->load->view('/themes/admin-footer');
	}

	public function update(){
		$enquiryId = $this->input->post('enquiryId', true);
		$currentStatus = $this->input->post('currentStatus', true);
		$this->Enquiry_model->updateEnquiry( array('status' =>$currentStatus), array('enquiry_id' =>$enquiryId ) );

		//For Displaying message in the next page
		$this->session->set_flashdata('enquiryUpdated', true);

		redirect('admin/enquiries');
	}
	public function delete(){
		$enquiryId = $this->input->post('enquiryId', TRUE);
		if($enquiryId){
			$this->Enquiry_model->deleteEnquiry( array('enquiry_id' => $enquiryId ) );

			//For Displaying message in the next page
			$this->session->set_flashdata('enquiryDeleted', true);

			redirect('admin/enquiries');
		}
	}
}// Controller ends here