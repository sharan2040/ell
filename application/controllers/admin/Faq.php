<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Faq extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Faq_model');
		$this->load->Model('Reminder_model');
	}

	public function index(){

		$data['faqs'] = $this->Faq_model->listFaq(false, false);
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/faq/list', $data);

	}

	public function add(){

		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/faq/add');

	}


	public function create(){
		$data = [
			'question' => $this->input->post('question', TRUE),
			'answer' => $this->input->post('answer', TRUE),
		];
		$this->Faq_model->createFaq( $data );

		//For Displaying message in the next page
		$this->session->set_flashdata('faqCreated', true);

		redirect('admin/faq');
	}

	public function edit($faqId){
		$data['faqs'] = $this->Faq_model->listFaq(false, array('faq_id' => $faqId ));
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/faq/edit', $data);
	}

	public function update(){

		$faqId = $this->input->post('faqId', TRUE);

		$data = [
			'question' => $this->input->post('question', TRUE),
			'answer' => $this->input->post('answer', TRUE),
		];

		$this->Faq_model->updateFaq( $data, array('faq_id' => $faqId) );

		//For Displaying message in the next page
		$this->session->set_flashdata('faqUpdated', true);

		redirect('admin/faq');
	}

	public function delete(){
		$faqId = $this->input->post('faqId', TRUE);
		if($faqId){
			$this->Faq_model->deleteFaq( array('faq_id' => $faqId ) );

			//For Displaying message in the next page
			$this->session->set_flashdata('faqDeleted', true);

			redirect('admin/faq');
		}
		else{
			redirect('admin/faq');
		}
	}

}