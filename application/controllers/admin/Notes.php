<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes extends Admin_Controller {

	
    public function __construct() {
        
        parent::__construct();

        $this->load->helper('url');
		$this->load->Model('Reminder_model');

    }



	public function index($noteId = NULL)
	{

		$this->load->model('Note_model');
		//If POST Request
		if ($this->input->post('note')) {


			$formData = [
				'note_content' => $this->input->post('note', TRUE), //TRUE : XSS Filter
				'created_on' => date("Y-m-d H:i:s")
			];

			$this->Note_model->createNote($formData);
		}

		$data['notes'] = $this->Note_model->listNotes(FALSE, FALSE);
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/note/list', $data);
		$this->load->view('/themes/admin-footer');	
	}

	public function unattended()
	{
		$this->load->helper('url');
		$this->load->view('/themes/admin-header');
		// $this->load->view('admin/dashboard');
		$this->load->view('/themes/admin-footer');	
	}

	public function all()
	{
		$this->load->helper('url');
		$this->load->view('/themes/admin-header');
		$this->load->view('admin/enquiry/list');
		$this->load->view('/themes/admin-footer');	
	}

	public function deleteNote($noteId = NULL)
	{
		if($noteId){
			$this->load->model('Note_model');
			$this->Note_model->deleteNote( array('note_id' => $noteId ) );

		//For Displaying message in the next page
		$this->session->set_flashdata('noteDeleted', true);

			redirect('admin/notes');
		}
		
	}

	

} // Controller Ends Here
