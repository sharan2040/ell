<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  
*/
class Projects extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();

		$this->load->model('Projects_model');
		$this->load->Model('Reminder_model');
	}

	public function index(){

		//Get Data from Model
		$data['projects'] = $this->Projects_model->listProjects(FALSE, FALSE);
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/project/list', $data);
	}

	public function add(){
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/project/add');

	}

	public function create(){
		//Get From Data
		$data = [
			'unique_id' => $this->input->post('uniqueId'),
			'customer_id' => $this->input->post('customerId'),
			'customer_name' => $this->input->post('customerName'),
			'project_details' => $this->input->post('projectDetails'),
			'current_status' => $this->input->post('customerStatus'),
			'created_date' => date("Y-m-d H:i:s")
		];

		//Insert into DB - model
		$this->Projects_model->createProject($data);

		//For Displaying message in the next page
		$this->session->set_flashdata('projectCreated', true);
		
		//redirect - ALWAYS TO A CONTROLLER METHOD AND NOT A VIEW
		redirect('admin/projects'); 
	}

	public function edit($trackingId){
		$data['projects'] = $this->Projects_model->listProjects(false, array('tracking_id' => $trackingId ));
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/project/edit', $data);
	}

	public function update(){

		$trackingId = $this->input->post('trackingId', TRUE);

		$data = [
			'customer_id' =>$this->input->post('customerId', true),
			'customer_name' => $this->input->post('customerName', true),
			'project_details' => $this->input->post('projectDetails', true),
			'current_status' => $this->input->post('customerStatus', true),
		];

		$this->Projects_model->updateProject( $data, array('tracking_id' => $trackingId) );

		//For Displaying message in the next page
		$this->session->set_flashdata('projectUpdated', true);

		redirect('admin/projects');
	}

	public function delete(){
		$trackingId = $this->input->post('trackingId', TRUE);
		if($trackingId){
			$this->Projects_model->deleteProject( array('tracking_id' => $trackingId ) );

			//For Displaying message in the next page
			$this->session->set_flashdata('projectDeleted', true);

			redirect('admin/projects');
		}
		else{
			redirect('admin/projects');
		}
		
	}
}// Controller Enda Here