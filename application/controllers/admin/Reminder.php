<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  
*/
class Reminder extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();

		$this->load->model('Reminder_model');
	}

	public function index(){

		//Get Data from Model
		$data['reminders'] = $this->Reminder_model->listReminder(FALSE, FALSE);
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('/themes/admin-header');
		$this->load->view('admin/reminder/list', $data);
	}

	public function add(){
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/reminder/add');

	}

	public function create(){
		//Get From Data
		$data = [
			'reminder' => $this->input->post('reminder'),
			'reminder_date' => date("Y-m-d", strtotime( $this->input->post('reminderDate') ) ),
		];
		//Insert into DB - model
		$this->Reminder_model->createReminder($data);

		//For Displaying message in the next page
		$this->session->set_flashdata('reminderCreated', true);
		
		//redirect - ALWAYS TO A CONTROLLER METHOD AND NOT A VIEW
		redirect('admin/reminder'); 
	}

	public function edit($reminderId){
		$data['reminders'] = $this->Reminder_model->listReminder(false, array('reminder_id' => $reminderId ));
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/reminder/edit', $data);
	}

	public function update(){

		$reminderId = $this->input->post('reminderId', TRUE);

		$data = [
			'reminder' => $this->input->post('reminder'),
			'reminder_date' => date("Y-m-d", strtotime( $this->input->post('reminderDate') ) ),
			];
		$this->Reminder_model->updateReminder( $data, array('reminder_id' => $reminderId) );

		//For Displaying message in the next page
		$this->session->set_flashdata('reminderUpdated', true);

		redirect('admin/reminder');
	}

	public function delete(){
		$reminderId = $this->input->post('reminderId', TRUE);
		if($reminderId){
			$this->Reminder_model->deleteReminder( array('reminder_id' => $reminderId ) );

			//For Displaying message in the next page
			$this->session->set_flashdata('reminderDeleted', true);

			redirect('admin/reminder');
		}
		else{
			redirect('admin/reminder');
		}
		
	}
}// Controller Enda Here