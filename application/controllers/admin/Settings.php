<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller{


	public function __construct(){

		parent::__construct();
		$this->load->model('Settings_model');
		$this->load->Model('Reminder_model');
	}

	public function index() {

		$data['settings'] = $this->Settings_model->getSettings(false, false);
		$headerData['reminders'] = $this->Reminder_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		//Load views + Pass Data
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/settings', $data);
		$this->load->view('/themes/admin-footer');	

	} 


	public function updateSettings(){

		$data = [
			'contact_email' => $this->input->post('contactEmail', TRUE),
			'contact_phone' => $this->input->post('contactNumber', TRUE),
			'contact_fax' => $this->input->post('contactFax', TRUE),
			'company_address' => $this->input->post('companyAddress', TRUE),
			'working_hours' => $this->input->post('workingHours', TRUE),
			'latitude' => $this->input->post('latitude', TRUE),
			'longitude' => $this->input->post('longitude', TRUE),
			'facebook_link' => $this->input->post('facebook', TRUE),
			'google_link' => $this->input->post('google', TRUE),
			'twitter_link' => $this->input->post('twitter', TRUE),
			'linkedin_link' => $this->input->post('linkedin', TRUE),
		];

		//Update Settings
		$this->Settings_model->updateSettings( $data, array('settings_id' => 1) );

		//For Displaying message in the next page
		$this->session->set_flashdata('settingsUpdated', true);

		//Redirect to next page
		redirect('admin/settings');

	}

}
