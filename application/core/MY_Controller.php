<?php

///////////////////////////
// ADMIN Base Controller //
///////////////////////////

class Admin_Controller extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('url');



    //Check for Session 
    if(!$this->session->userdata('user_id')){
      redirect('admin/login');
    }
  
  }


} //Controller Ends here





		