<?php
/**
 * Description    : Model to Manage Enquiries
 * Created        : 30-05-16
 */

 class Blog_Model extends CI_Model{
 	
 	public function __construct(){
 		parent:: __construct();
 	}
 
 	public function createBlog($data){
 		if($data){
 			$this->db->insert('blog', $data);
 			return TRUE;
 		}
 	}

 	public function listBlogs($fields = FALSE, $conditions = FALSE, $limit = FALSE){
 		if($fields != FALSE){
 			$this->db->select($fields);
 		}
 		$this->db->from('blog');

 		if($conditions != FALSE){
 			$this->db->where($conditions);
 		}

        if($limit != FALSE){
            $this->db->limit($limit);
        }


 		$this->db->order_by('blog_id', 'desc');
 		$query = $this->db->get();
 		return $query->result();
 	}

 	public function updateBlog($fields = FALSE, $conditions){

        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('blog', $fields); 
                return TRUE;
            }

        } 
        catch (Exception $e) {
            echo $e->getMessage();
        }
 	}

 	public function deleteBlog($condition = FALSE){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('blog', $condition); 
                return TRUE;
           } else {
                return FALSE;
           }

        } 
        catch (Exception $e) {
            echo $e->getMessage();
        }	
    }

    public function incrementView($slug = FALSE){
        try {

           if( $slug!= FALSE ){
                $this->db->where('slug', $slug);
                $this->db->set('views', 'views+1', FALSE);
                $this->db->update('blog');

                return TRUE;
           } else {
                return FALSE;
           }

        } 
        catch (Exception $e) {
            echo $e->getMessage();
        }   
    }


 }//Model Ends Here