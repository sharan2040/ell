<?php

/**
 * Description    : Model to Manage Enquiries
 * Created        : 22-01-16
 */

class Career_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function createCareer($postData){

    	if($postData){
    		$this->db->insert('careers', $postData);
    		return TRUE;
    	}

    }

    public function listCareers($fields = FALSE, $conditions = FALSE)
    {
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('careers');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $this->db->order_by('career_id', 'desc');

        $query = $this->db->get();
        return  $query->result();
    }

    /**
     * @uses    Update a Product
     * @param   fields[array], conditions[array]
     * @return  boolean
     */
   public function updateCareer($fields = FALSE, $conditions)
    {
        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('careers', $fields); 
                return TRUE;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @uses    Delete a Brand
     * @param   conditions (array)
     * @return  boolean
     */
    public function deleteCareer( $conditions = FALSE )
    {
        try {

           if( $conditions!= FALSE ){
                $this->db->delete('careers', $conditions); 
                return TRUE;
           } else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }




}
