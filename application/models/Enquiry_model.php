<?php

/**
 * Description    : Model to Manage Enquiries
 * Created        : 22-01-16
 */

class Enquiry_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }


    public function createEnquiry($postData){

    	if($postData){
    		$this->db->insert('enquiries', $postData);
    		return TRUE;
    	}

    }

    public function listEnquiries($fields = FALSE, $conditions = FALSE)
    {
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('enquiries');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $this->db->order_by('enquiry_id', 'desc');

        $query = $this->db->get();
        return  $query->result();
    }

    /**
     * @author  Toobler
     * @uses    Update a Product
     * @param   fields[array], conditions[array]
     * @return  boolean
     */
   public function updateEnquiry($fields = FALSE, $conditions)
    {
        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('enquiries', $fields); 
                return TRUE;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @uses    Delete a Brand
     * @param   conditions (array)
     * @return  boolean
     */
    public function deleteEnquiry( $conditions = FALSE )
    {
        try {

           if( $conditions!= FALSE ){
                $this->db->delete('enquiries', $conditions); 
                return TRUE;
           } else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }




}
