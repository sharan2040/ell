<?php

/**
 * Description    : Model to Manage Enquiries
 * Created        : 06-06-16
 */

class Faq_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function createFaq($postData){

    	if($postData){
    		$this->db->insert('faq', $postData);
    		return TRUE;
    	}

    }

    public function listFaq($fields = FALSE, $conditions = FALSE)
    {
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('faq');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $this->db->order_by('order', 'desc');

        $query = $this->db->get();
        return  $query->result();
    }

    /**
     * @uses    Update a Product
     * @param   fields[array], conditions[array]
     * @return  boolean
     */
   public function updateFaq($fields = FALSE, $conditions)
    {
        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('faq', $fields); 
                return TRUE;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @uses    Delete a Brand
     * @param   conditions (array)
     * @return  boolean
     */
    public function deleteFaq( $conditions = FALSE )
    {
        try {

           if( $conditions!= FALSE ){
                $this->db->delete('faq', $conditions); 
                return TRUE;
           } else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }




}
