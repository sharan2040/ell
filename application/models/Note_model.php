<?php

/**
 * Author         : Toobler
 * Company        : Toobler Technologies
 * Project        : SelfiePleasure
 * Description    : Model to Manage Users
 * Created        : 22-01-16
 */

class Note_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function createNote($postData){

    	if($postData){
    		$this->db->insert('notes', $postData);
    		return TRUE;
    	}

    }

    public function listNotes($fields = FALSE, $conditions = FALSE)
    {
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('notes');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $this->db->order_by('note_id', 'desc');

        $query = $this->db->get();
        return  $query->result();
    }

    /**
     * @uses    Delete a Brand
     * @param   conditions (array)
     * @return  boolean
     */
    public function deleteNote( $conditions = FALSE )
    {
        try {

           if( $conditions!= FALSE ){
                $this->db->delete('notes', $conditions); 
                return TRUE;
           } else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }




}
