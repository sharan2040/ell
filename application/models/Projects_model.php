<?php 
/**
 * Description    : Model to Manage Projetcs
 * Created        : 29-05-16
 */

class Projects_model extends CI_model{
    public function __construct()
    {
        parent::__construct();
    }

	public function createProject($data){
		if($data){
			$this->db->insert('project_tracking', $data);
			return TRUE;
		}
	}

	public function listProjects($fields = FALSE, $conditions = FALSE){
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('project_tracking');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $this->db->order_by('customer_id', 'desc');

        $query = $this->db->get();
        return  $query->result();
	}

   public function updateproject($fields = FALSE, $conditions)
    {
        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('project_tracking', $fields); 
                return TRUE;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    } 

	public function deleteProject($condition = FALSE){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('project_tracking', $condition); 
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
    }
}