<?php

/**
 * Description    : Model to Manage Enquiries
 * Created        : 22-01-16
 */

class Quote_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function createQuote($postData){

    	if($postData){
    		$this->db->insert('quotes', $postData);
    		return TRUE;
    	}

    }

    /**
     * @author  Sharan Mohandas
     * @uses    To Insert/Update Offer | Adds offer amount to Chat
     * @param   $data [array] - Offer Data
     * @return  boolean/insert_id
    */
    public function upsertQuote($data){
        
        try {

            $data['created_date'] = date("Y-m-d H:i:s");

            $this->db->select('quote_id');
            $this->db->where('enquiry_id', $data['enquiry_id']);
            $quote = $this->db->get('quotes')->row();

            if($quote){ //If Quote present Update
                $this->db->where('quote_id',$quote->quote_id);
                $this->db->update('quotes',$data);
                return TRUE;
            }
            else { // Insert New
                $this->db->insert('quotes', $data);
                return $this->db->insert_id();
            }

        } catch (Exception $e) {
             echo $e->getMessage();
        }
    }


    public function listQuotes($fields = FALSE, $conditions = FALSE)
    {
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('quotes');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $this->db->order_by('quote_id', 'desc');

        $query = $this->db->get();
        return  $query->result();
    }

    /**
     * @uses    Delete a Brand
     * @param   conditions (array)
     * @return  boolean
     */
    public function deleteQuote( $conditions = FALSE )
    {
        try {

           if( $conditions!= FALSE ){
                $this->db->delete('quotes', $conditions); 
                return TRUE;
           } else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }






}
