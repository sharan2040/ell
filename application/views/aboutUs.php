      <!-- Page Title -->
      <section class="page-title">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>About Us</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
				<li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
            	<li><a class="pathway">About</a></li>
				<li class="active">Company Profile</li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  

	  <!-- Top A -->
      <section class="top-a">
        <div class="container">
		  <div class="row">
		    <div class="col-sm-12 col-md-12">
			  <div class="module title3">
			    <h3 class="module-title">Meet Our Team</h3>
			    <div class="module-content">
	              <!-- <p><img width="1170" height="300" alt="Meet Our Team" src="<?=base_url()?>assets/default/images/business_team.jpg"></p> -->
                  <br>
				  <div class="uk-grid uk-margin-bottom">
                    <div class="uk-width-medium-1-2">
                      <p class="dropcap">Elegant Line Logistics LLC started in the 2011 specialises in road freight delivery and value added service to your supply chain throughout Dubai and the Middle East. As the trusted choice for some of UAE's biggest retail brands, our focus is on reliability and efficiency, ensuring your cargo arrives safely on time. Over the past 5 years we have made a priority to stay up to date with market-leading technology, constantly improving our processes so as to offer the best service possible to our clients. ELL offers comprehensive and cost effective solution to your entire packing and removals requirement. We provide total door to door relocation service worldwide with our network and service partners servicing our clients at their door step to provide them hassle free relocation service with utmost care. </p>
                    </div>
                    <div class="uk-width-medium-1-2">From packing to transit and delivery, our professionals provide a high level of customer service and support to ensure your move is cost-effective, efficient and secure. By combining our full range of value-adding services, together with the expertise and passion of our experienced team, we are able to provide our clients with ongoing improvement, continually strengthening our offering to you. Technology is tailored on a case by case basis, offering flexibility to clients of all sizes and with all volumes of freight. We offer online integration with a full IT support team ready to meet your needs. At ELL we believe in fostering fruitful, long term relationships that enable our clients to extend their business across borders in the most reliable and efficient way possible. We have agreement with following associations
                    </div>
                  </div>
                </div>
			  </div>
			</div>
		  </div>
		  <!-- <div class="row"> -->
		 <!--    <div class="col-sm-4 col-md-4">d</div>
		    <div class="col-sm-4 col-md-4">d</div>
		    <div class="col-sm-4 col-md-4">d</div> -->
		  <!-- </div> -->
		</div>
	  </section>	  
	  <!-- /Top A -->

	  <!-- Top B -->
      <section class="top-b">
	    <div class="container">
		  <div class="row">
		    <div class="col-sm-12 col-md-12">
              <div class="module title3">
                <h3 class="module-title">Why To Choose Us</h3>
                <div class="module-content">
	              <div class="uk-grid uk-text-center">
                    <div class="uk-width-medium-1-3">
                      <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-panel-hover">
                        <i class="pe-7s-portfolio pe-4x pe-va"></i>
                        <h4>Professionals</h4>
                      </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                      <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-panel-hover">
                        <i class="pe-7s-vector pe-4x pe-va"></i>
                        <h4>5 Years of Experience</h4>
                      </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                      <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-panel-hover">
                        <i class="pe-7s-rocket pe-4x pe-va"></i>
                        <h4>Quick Delivery</h4>
                      </div>
                    </div>
                  </div>
                  <div class="uk-grid uk-text-center">
                    <div class="uk-width-medium-1-3">
                      <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-panel-hover">
                        <i class="pe-7s-magic-wand pe-4x pe-va"></i>
                        <h4>Trained Worksman</h4>
                      </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                      <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-panel-hover">
                        <i class="pe-7s-tools pe-4x pe-va"></i>
                        <h4>Quality Tools &amp; Products</h4>
                      </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                      <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-panel-hover">
                        <i class="pe-7s-ribbon pe-4x pe-va"></i>
                        <h4>Warranty &amp; Insurance</h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
			</div>
		  </div>
		</div>
	  </section>	  
	  <!-- /Top B -->	  
	  
	  
	  
	  <!-- Testimonials -->
	  <!-- 
      <section class="main-body position-a">
	    <div class="container">
		  <div class="row">
		    <div class="col-sm-12 col-md-12">
			  <div class="module title1">
			    <h3 class="module-title">Testimonials</h3>
                <div data-uk-slideset="{small: 1, medium: 2}">
                  <ul class="uk-grid uk-slideset customers">
		            <li>
	                  <div class="sprocket-quotes-item quotes-bottomleft-arrow">
				        <div class="sprocket-quotes-info">
						  <img src="<?=base_url()?>assets/default/images/elements/testimonial1.png" class="sprocket-quotes-image" alt="">
					    </div>
						<div class="sprocket-quotes-author">
				          <h4><strong>Jeremy Diaz</strong></h4>
			            </div>
						<span class="sprocket-quotes-subtext">Manager</span>
						<div class="sprocket-quotes-text">
				          <h5 style="line-height: 1.9em;">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</h5>
			            </div>
						<a href="#" class="readon"><span>Read More</span></a>
			          </div>
                    </li>
                    <li>
	                  <div class="sprocket-quotes-item quotes-bottomleft-arrow">
				        <div class="sprocket-quotes-info">
						  <img src="<?=base_url()?>assets/default/images/elements/testimonial2.png" class="sprocket-quotes-image" alt="">
					    </div>
						<div class="sprocket-quotes-author">
				          <h4><strong>Amanda Pierce</strong></h4>
			            </div>
						<span class="sprocket-quotes-subtext">Housewife</span>
						<div class="sprocket-quotes-text">
				          <h5 style="line-height: 1.9em;">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</h5>
			            </div>
						<a href="#" class="readon"><span>Read More</span></a>
			          </div>
                    </li>
                    <li>
	                  <div class="sprocket-quotes-item quotes-bottomleft-arrow">
				        <div class="sprocket-quotes-info">
						  <img src="<?=base_url()?>assets/default/images/elements/testimonial3.png" class="sprocket-quotes-image" alt="">
					    </div>
						<div class="sprocket-quotes-author">
				          <h4><strong>Anton Georgiev</strong></h4>
			            </div>
						<span class="sprocket-quotes-subtext">IT Specialist</span>
						<div class="sprocket-quotes-text">
				          <h5 style="line-height: 1.9em;">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</h5>
			            </div>
						<a href="#" class="readon"><span>Read More</span></a>
			          </div>
                    </li>
                    <li>
	                  <div class="sprocket-quotes-item quotes-bottomleft-arrow">
				        <div class="sprocket-quotes-info">
						  <img src="<?=base_url()?>assets/default/images/elements/testimonial4.png" class="sprocket-quotes-image" alt="">
					    </div>
						<div class="sprocket-quotes-author">
				          <h4><strong>Jenna Zane</strong></h4>
			            </div>
						<span class="sprocket-quotes-subtext">Manager</span>
						<div class="sprocket-quotes-text">
				          <h5 style="line-height: 1.9em;">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</h5>
			            </div>
						<a href="#" class="readon"><span>Read More</span></a>
			          </div>
                    </li>						
                  </ul>
				  <div class="uk-grid">
				    <div class="uk-width-medium-1-5 uk-push-2-5 uk-flex uk-flex-center" >
                      <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>							
                      <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>						  
				    </div>							  							  
                  </div>						  
                </div>				
              </div>
		    </div>
		  </div>
	    </div>
	  </section>	
	   -->  
	  <!-- /Testimonials -->	  

	  <!-- Bottom -->
	  <!--  
      <section class="bottom">
	    <div class="container">
		  <div class="row">
		    <div class="col-sm-6 col-md-12">
		      <div class="module title1">
                <h3 class="module-title">Our Clients</h3>
                <div class="module-content">
	              <div class="uk-grid">
                    <div class="uk-width-medium-1-5">
                      <img src="<?=base_url()?>assets/default/images/elements/logo1.png" alt="Our Clients" class="uk-align-center">
                    </div>
                    <div class="uk-width-medium-1-5">
                      <img src="<?=base_url()?>assets/default/images/elements/logo2.png" alt="Our Clients" class="uk-align-center">
                    </div>
                    <div class="uk-width-medium-1-5">
                      <img src="<?=base_url()?>assets/default/images/elements/logo3.png" alt="Our Clients" class="uk-align-center">
                    </div>
                    <div class="uk-width-medium-1-5">
                      <img src="<?=base_url()?>assets/default/images/elements/logo4.png" alt="Our Clients" class="uk-align-center">
                    </div>
                    <div class="uk-width-medium-1-5">
                      <img src="<?=base_url()?>assets/default/images/elements/logo5.png" alt="Our Clients" class="uk-align-center">
                    </div>
                  </div>
                </div>
              </div>
		    </div>
		  </div>
		</div>
	  </section>	
	  -->  
	  <!-- /Bottom -->