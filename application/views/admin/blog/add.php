<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">




<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">News & Events</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">

				<div class="card">
					<div class="card-block">
						<form method="POST" action="<?php echo base_url()?>admin/blog/create" enctype="multipart/form-data" >
 							<button type="submit" class="btn btn-primary pull-right">Create</button><div class="clearfix"></div>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">News Title</label>
								<input type="text" class="form-control" name="blogTitle">
								<small class="text-help">News HeadLines</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Cover Image</label>
								<input type="file" class="form-control" name="coverImage" >
								<small class="text-help"><strong>Recommended Resolution : 1170x700 | Formats : JPG,PNG |</strong> <i class="fa fa-dot-circle-o" aria-hidden="true"></i> Please to stick to the recommendation to ensure the blog looks perfect and well-structured</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Contents</label>
									<textarea id="blogText" name="contents"></textarea>
								<small class="text-help">Detailed Explanation of the News/Blog | Supports formatting (Bold, Italics etc..)</small>
							</fieldset>

					</div>
				</div>


			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#blogText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



