 <!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">


<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Careers</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">


				<div class="card">
					<div class="card-block">
						<h5>Edit Careers</h5>
						<form method="POST" action="<?php echo base_url()?>admin/careers/updateCareer" style="display:inline;">

							<!-- PASSING id -->
							<input type="hidden" name="careerId" value="<?= $careers[0]->career_id;?>">

 							<fieldset class="form-group">
								<label for="exampleInputEmail1">Job Title</label>
								<input type="text" class="form-control" value="<?= $careers[0]->job_title;?>" name="jobTitle">
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Job Location</label>
								<input type="text" class="form-control" value="<?= $careers[0]->job_location;?>" name="jobLocation">
							</fieldset>

							<fieldset class="form-group">
								<label>Job Description</label>
								<div class="form-group">
									<textarea id="jobDesc" name="jobDescription"><?= $careers[0]->job_description;?></textarea>
								</div>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Appn. End Date</label>
								<input type="text" class="datepicker form-control" value="<?= date('m/d/Y', strtotime($careers[0]->application_enddate));?>" name="applicationEndDate">
							</fieldset>

 							<button type="submit" class="btn btn-primary">Update</button>
						</form>
						<form method="POST" action="<?php echo base_url()?>admin/careers/deleteCareer" style="display:inline;">
							<input type="hidden" name="careerId" value="<?= $careers[0]->career_id;?>">
 							<button type="submit" class="btn btn-danger">Delete</button>
 						</form>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#jobDesc').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



