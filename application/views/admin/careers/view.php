<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">


<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<!-- For Displaying Notification for Creating career-->
		<?php if( $this->session->flashdata('careerCreated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Career Created.
			</div>
		<?php endif;?>

		<!-- For Displaying Notification for delete career-->
		<?php if( $this->session->flashdata('careerDeleted') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Career Deleted.
			</div>
		<?php endif;?>

		<!-- For Displaying Notification to update career-->
		<?php if( $this->session->flashdata('careerUpdated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Career Updated.
			</div>
		<?php endif;?>


		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Careers</li>
		</ol>

		<div class="row">
			<div class="col-xl-12">

				<div class="card">
					<div class="card-block">
						<h5>Current Postings</h5>
						<table id="enquiryTable" class="table table-striped table-hover table-sm">
							<thead>
								<tr>
									<th>Job ID</th>
									<th>Job Title</th>
									<th>Job Location</th>
									<th>Appln. End Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>

								<?php foreach($careers as $career) : ?>
								<tr>
									<td><?= $career->career_id?></td>
									<td><?= $career->job_title?></td>
									<td><?= $career->job_location?></td>
									<td><?= $career->application_enddate?></td>
									<td><a href="<?=base_url()?>admin/careers/edit/<?= $career->career_id?>">Edit/Delete</a></td>
								</tr>
								<?php endforeach; ?>

							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>

				<div class="card">
					<div class="card-block">
						<h5>Add new Job Opening</h5>
						<form method="POST" action="<?php echo base_url()?>admin/careers/create">
							<fieldset class="form-group">
								<label for="exampleInputEmail1">Job Title</label>
								<input type="text" class="form-control" name="jobTitle">
								<small class="text-help">Eg : Sr. Sales Manager</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Job Location</label>
								<input type="text" class="form-control" name="jobLocation">
								<small class="text-help">Eg : Karama | Dubai | UAE</small>
							</fieldset>

							<fieldset class="form-group">
								<label>Application End Date</label>
								<div class="form-group">
									<input class="datepicker form-control" name="endDate" type="text" value="<?php echo date('m/d/Y')?>" />
								</div>
								<small class="text-help">Job will be hidden from the main website after end date.</small>
							</fieldset>

							<fieldset class="form-group">
								<label>Brief Description</label>
								<div class="form-group">
									<textarea id="careersText" name="jobDescription"></textarea>
								</div>
							</fieldset>

							<button type="submit" class="btn btn-primary">Add Now</button>
						</form>
					</div>
				</div>

			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#careersText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



