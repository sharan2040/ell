   <!-- Charts CSS -->
   <link rel="stylesheet" href="http://localhost/ci-skeleton/assets/admin/examples/css/morris.min.css">


   <!-- Content -->
   <div class="layout-content" data-scrollable>
    <div class="container-fluid">     

      <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li><a href="">Home</a></li>
        <li><a href="#">Dashboard</a></li>
      </ol>

      <!-- Row -->

      <div class="row">


      </div>
      <!-- // END Row -->

      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="card-block">
              <div id="donut" style="width: 100%; height:230px;"></div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-block">
              <div id="bar" style="width: 100%; height:230px;"></div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- Theme Colors -->
  <script src="<?=base_url()?>assets/admin/assets/js/colors.js"></script>

  <!-- Charts JS -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/raphael-min.js"></script>
  <script src="<?=base_url()?>assets/admin/assets/vendor/morris.min.js"></script>

  <!-- Initialize Charts -->

  <script>
  (function ($) {

    if ($('#donut').length) {
      new Morris.Donut({
        element: 'donut',
        data: [
        { label: "New/Unattended Enquiries", value: <?= $new_enquiries_count?> },
        { label: "Rejected Enquiries", value: <?= $rejected_enquiries_count?> },
        { label: "Attended Enquiries", value: <?= $attended_enquiries_count?> }
        ],
        colors: [ colors[ 'chart-primary' ],  colors[ 'chart-secondary' ],  colors[ 'chart-third' ]],
        resize: true
      });
    }

    if ($('#bar2').length) {
    new Morris.Bar({
      element: 'bar2',
      data: [
        { y: '2006', a: 100 },
        { y: '2007', a: 75 },
        { y: '2008', a: 50 },
        { y: '2009', a: 75 },
        { y: '2010', a: 50 },
        { y: '2011', a: 75 },
        { y: '2012', a: 100 },
        { y: '2013', a: 200 },
        { y: '2014', a: 300 },
        { y: '2015', a: 260 },
        { y: '2016', a: 40 }
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Sales'],
      barColors: [ colors[ 'chart-primary' ] ],
      barShape: 'soft',
      xLabelMargin: 10,
      resize: true
    });
  }

  if ($('#bar').length) {
    new Morris.Bar({
      element: 'bar',
      data: [
        { y: 'January', a: 100 },
        { y: 'February', a: 75 },
        { y: 'March', a: 50 },
        { y: 'April', a: 75 },
        { y: 'May', a: 90 },
        { y: 'June', a: 50 },
        { y: 'July', a: 75 },
        { y: 'August', a: 100 },
        { y: 'September', a: 200 },
        { y: 'October', a: 300 },
        { y: 'November', a: 260 },
        { y: 'December', a: 40}
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Sales'],
      barColors: [ colors[ 'chart-primary' ] ],
      barShape: 'soft',
      xLabelMargin: 10,
      resize: true
    });
  }

  if ($('#line1').length) {
    new Morris.Line({
      element: 'line1',
      data: [
        { y: '2008', a: 150, b:50 },
        { y: '2009', a: 75, b:90 },
        { y: '2010', a: 200, b:120 },
        { y: '2011', a: 75, b:340 },
        { y: '2012', a: 130, b:60 }
      ],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['New', 'Resolved'],
      lineColors: [ colors[ 'chart-primary' ], colors[ 'chart-secondary' ]],
      resize: true
    });
  }

  }(jQuery));
 </script>


