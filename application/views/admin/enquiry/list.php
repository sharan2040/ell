 <link rel="stylesheet" href="https://cdn.jsdelivr.net/fontawesome/4.5.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">
 <!-- Content -->
 <div class="layout-content" data-scrollable>
  <div class="container-fluid">

    <!-- For Displaying Notification for Updating Status of Enquiry-->
    <?php if( $this->session->flashdata('enquiryUpdated') == true ): ?>
      <div class="alert alert-success alert-fixed">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Enquiry Updated.
      </div>
    <?php endif;?>

    <!-- For Displaying Notification for Enquiry Deletion-->
    <?php if( $this->session->flashdata('enquiryDeleted') == true ): ?>
      <div class="alert alert-success alert-fixed">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Enquiry Deleted.
      </div>
    <?php endif;?>

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
      <li><a href="#">Dashboard</a></li>
      <li><a href="#">Enquiries</a></li>
      <li class="active">All</li>
    </ol>

    <!-- Row -->
    <div class="row">
      <!-- Column -->
      <div class="col-md-12">

        <div class="card">
          <div class="card-header">
            <h5 class="card-title">All Enquiries</h5>
          </div>
          <table id="enquiryTable" class="table table-striped table-hover table-sm">
            <thead>
              <tr>
                <th width="8%">Enq ID</th>
                <th>Name</th>
                <th>Company</th>
                <th>Email</th>
                <th width="20%">Enquiry Date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($enquiries as $enquiry) : ?>
              <tr>
                <td><?= $enquiry->enquiry_id; ?></td>
                <td><?= $enquiry->client_name; ?></td>
                <td><?= $enquiry->client_company; ?></td>
                <td><?= $enquiry->client_email; ?></td>
                <td><?= $enquiry->created_date; ?></td>
                <td><?= $enquiry->status; ?></td>
                <td><a href="<?=base_url()?>admin/Enquiries/viewEnquiries/<?= $enquiry->enquiry_id?>">View/ Delete</a></td>
              </tr>
              <?php endforeach; ?>              
            </tbody>
          </table>
          <div class="clearfix"></div>
        </div>

      </div>
      <!-- // END Column -->

    </div>
  </div>

  <!-- Vendor JS -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
  <script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>

  <script>
  $('#enquiryTable').DataTable({
      "order": [[ 0, "desc" ]]
  });
  </script>