<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">




<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Enquiries</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">
			DESIGN WORK IN PROGRESS FOR THIS PAGE
				<div class="card">
					<div class="card-block">
						Name : <?=$enquiries[0]->client_name ?><br>
						Email : <?=$enquiries[0]->client_email ?><br>
						Company : <?=$enquiries[0]->client_company ?><br>
						Phone Number : <?=$enquiries[0]->phone_number ?><br>
						Date Of Move : <?=$enquiries[0]->date_of_move ?><br>
						Zip Code : <?=$enquiries[0]->zip_code ?><br>
						Type Of Project : <?=$enquiries[0]->type_of_project ?><br>
						Square Footage : <?=$enquiries[0]->square_footage ?><br>
						Description : <?=$enquiries[0]->description ?><br>
						Created Date : <?=$enquiries[0]->created_date ?><br>
							<form method="POST" action="<?php echo base_url()?>admin/enquiries/update" style="display:inline;">
							<input type="hidden" name="enquiryId" value="<?=$enquiries[0]->enquiry_id ?>">
							<select name="currentStatus">
								<option value="0" <?php if($enquiries[0]->status == '0') echo "Selected"?>>Unattended</option>
								<option value="1" <?php if($enquiries[0]->status == '1') echo "Selected"?>>Attended</option>
								<option value="2" <?php if($enquiries[0]->status == '2') echo "Selected"?>>Rejected</option>
							</select>
 							<button type="submit" class="btn btn-primary">Update</button>
							</form>
							
							<form method="POST" action="<?php echo base_url()?>admin/enquiries/delete" style="display:inline;">
							<input type="hidden" name="enquiryId" value="<?=$enquiries[0]->enquiry_id ?>">
 							<button type="submit" class="btn btn-danger">Delete</button>
							</form>
					</div>
				</div>


			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#careersText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



