<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">


<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<!-- For Displaying Notification for Creating FAQ-->
		<?php if( $this->session->flashdata('faqCreated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> FAQ Created.
			</div>
		<?php endif;?>

		<!-- For Displaying Notification for delete FAQ-->
		<?php if( $this->session->flashdata('faqDeleted') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> FAQ Deleted.
			</div>
		<?php endif;?>

		<!-- For Displaying Notification to update FAQ-->
		<?php if( $this->session->flashdata('faqUpdated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> FAQ Updated.
			</div>
		<?php endif;?>


		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">FAQ</li>
		</ol>

		<div class="row">
			<div class="col-xl-12">

				<a href="<?php echo base_url()?>admin/faq/add"><button type="button" class="btn btn-primary pull-right">Add New</button></a><div class="clearfix"></div><br>

				<div class="card">
					<div class="card-block">
						<h5>Manage FAQs</h5>
						<table id="enquiryTable" class="table table-striped table-hover table-sm">
							<thead>
								<tr>
									<th>Questions</th>
									<th>Answers</th>
									<!-- <th>Priorty</th> -->
									<th width="15%">Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($faqs as $faq) : ?>
								<tr>
									<td><?= $faq->question?></td>
									<td><?= substr($faq->answer, 0 , 200)."...";?></td>
									<!-- <td><?= $faq->order?></td> -->
									<td><?= $faq->created_date?></td>
									<td><a href="<?=base_url()?>admin/faq/edit/<?= $faq->faq_id?>">Edit/Delete</a></td>
								</tr>
								<?php endforeach; ?>							
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>


			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#careersText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



