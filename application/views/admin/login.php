<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Login/Signup</title>

  <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
  <meta name="robots" content="noindex">

  <!-- Material Design Icons  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Roboto Web Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">

  <!-- App CSS -->
  <link type="text/css" href="<?=base_url()?>assets/admin/assets/css/style.min.css" rel="stylesheet">

  <style>
  body{
      background-color: rgba(0,43,86,1);
  }
  </style>

</head>

<body class="login">
<br>  

<?php if ($this->session->flashdata('loginFailed') == TRUE) :?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">×</span>
  </button>
  <strong>Holy guacamole!</strong> <?php echo $this->session->flashdata('message'); ?>
</div>
<?php endif ;?>


  <div class="row">
    <div class="col-sm-10 col-sm-push-1 col-md-4 col-md-push-4 col-lg-4 col-lg-push-4">
      <h2 class="text-primary center m-a-2">
        <img src="<?=base_url()?>assets/admin/assets/images/689.png" height="50" >
      </h2>
      <div class="card-group">
        <div class="card">
          <div class="card-block">
            <div class="center">
              <h4 class="m-b-0"><span class="icon-text">Login</span></h4>
              <p class="text-muted">Access your account</p>
            </div>
            <form action="<?=base_url()?>admin/login" method="post">
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email Address">
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <a href="#" class="pull-xs-right">
                  <small>Forgot?</small>
                </a>
                <div class="clearfix"></div>
              </div>
              <div class="center">
                <button type="submit" class="btn  btn-primary-outline btn-circle-large">
                  <i class="material-icons">lock</i>
                </button>
              </div>
            </form>
          </div>
        </div>
        
      </div>
    </div>
  </div>

  <!-- jQuery -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/jquery.min.js"></script>

  <!-- Bootstrap -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
  <script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

  <!-- AdminPlus -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

  <!-- App JS -->
  <script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

</body>
</html>