 <!-- Sidebar -->
 <div class="sidebar sidebar-right si-si-3 sidebar-visible-md-up sidebar-light bg-white ls-top-navbar-xs-up" id="sidebarRight" data-position="right" data-scrollable>

 	<!-- Sidebar Notes -->
 	<div class="sidebar-block center" >
 		<a href="#" id="saveButton" class="btn btn-success btn-sm btn-rounded-deep">
 			Save <i class="material-icons md-18">save</i>
 		</a>
 		<a href="#" id="clearButton" class="btn btn-warning btn-sm btn-rounded-deep">
 			New Note <i class="material-icons md-18">add</i>
 		</a>
 	</div>
 	<br>
 	<div class="sidebar-heading">My notes</div>
 	<ul class="sidebar-filter">
 		<?php foreach($notes as $note):?>
 			<li><a href="#" style="display:inline" class="viewLink" data-note="<pre><?= $note->note_content; ?></pre>" data-noteAdded="Added on <?php echo date('d M | l', strtotime($note->created_on) )?>" ><i class="material-icons text-primary">lens</i> <?= substr($note->note_content,0,15).".." ?> </a><a style="display:inline;" class="deleteNote" data-noteId="<?= $note->note_id; ?>" ><i class="material-icons" style="font-size:18px;">delete</i></a></li>
 		<?php endforeach; ?>
 	</ul>
 	<!-- // END Sidebar Notes -->

 </div>


 <!-- Content -->
 <div class="layout-content" data-scrollable>
 	<div class="container-fluid">

		<!-- For Displaying Notification for Deleting note-->
		<?php if( $this->session->flashdata('noteDeleted') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Note Deleted.
			</div>
		<?php endif;?>

 		<ol class="breadcrumb">
 			<li><a href="#">Home</a></li>
 			<li class="active">Notes</li>
 		</ol>
 		<form id="noteForm" action="<?=base_url()?>admin/notes/index" method="POST">
	 		<div id="note">
	 			<textarea autofocus id="noteTextArea" name="note" class="form-control" placeholder="Write something awesome!" rows="30"></textarea>
	 		</div>
 		</form>
 		
 	</div>
 </div>

 <!-- Vendor CSS -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/sweetalert.min.css">

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/sweetalert.min.js"></script>

<!-- Initialize -->
<script src="<?=base_url()?>assets/admin/examples/js/sweetalert.js"></script>

<script>
	$("#saveButton").click(function() {
  		$('#noteForm').submit();
    });

 //    $('body').on('click', '.viewLink', function () {
	// 	alert($(this).attr('data-noteId'));
	// });

    $('#clearButton').click("focus", function() {
	  $('#noteTextArea').val('');
	});

	$('body').on('click', '.viewLink', function () {
		swal({
			title: $(this).attr('data-noteAdded'),
			text: $(this).attr('data-note'),
			html: true,
			confirmButtonColor: "#039BE5"
		});
	});


	$('body').on('click', '.deleteNote', function () {
		var noteId = $(this).attr('data-noteId');
        var baseUrl ='<?php echo base_url();?>';

		swal({ 
			title: "Are you sure?", 
			text: "You will not be able to recover this Note!", 
			type: "warning", 
			showCancelButton: true, 
			confirmButtonColor: "#039BE5", 
			confirmButtonText: "Yes, delete it!", 
			closeOnConfirm: false 
		}, function() { 

	        setTimeout(  window.location.href=baseUrl+"admin/notes/deleteNote/"+noteId , 2000);

		});
	});

</script>
