<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">




<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Projects</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">

				<div class="card">
					<div class="card-block">
						<form method="POST" action="<?php echo base_url()?>admin/projects/create">
 							<button type="submit" class="btn btn-primary pull-right">Create</button><div class="clearfix"></div>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Unique ID</label>
								<input type="text" class="form-control" name="uniqueId" value="<?= strtoupper(bin2hex(openssl_random_pseudo_bytes(5)))?>" required>
								<small class="text-help">This ID is generated randomly | This is unique and should be provided to the customer for tracking this Consignment's details | <i class="fa fa-check-circle" aria-hidden="true"></i> If you have generated your own ID replace the above | <i class="fa fa-times-circle" aria-hidden="true"> </i> ID Once created cannot be updated</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Customer ID</label>
								<input type="text" class="form-control" name="customerId">
								<small class="text-help"><i class="fa fa-info-circle" aria-hidden="true"></i> For your records only | Not displayed in the frontend</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Customer Name</label>
								<input type="text" class="form-control" name="customerName" required>
								<small class="text-help"><i class="fa fa-info-circle" aria-hidden="true"></i> For your records only | Not displayed in the frontend</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Current Status</label>
								<textarea id="projectText" name="projectDetails" required>DEMO STATUS TEMPLATE<p><span xss="removed"><font face="Arial" color="#6ba54a"><b>08/05/2016 - Goods at Warehouse (Current Status)</b></font></span></p><p><font face="Arial"><span xss="removed">05/05/2016 - On Transit</span><br></font></p><p><font face="Arial">05/05/2016 - Your consignment has been packed</font></p><p><font face="Arial">01/05/2016 - Order Confirmed</font><br></p></textarea>
								<small class="text-help">Current Status of the Project | <i class="fa fa-check-circle" aria-hidden="true"></i> Displayed to the customer in the Frontend</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Project Details</label>
								<textarea id="customerStatus" class="form-control" name="customerStatus"></textarea>
								<small class="text-help">Details about the project | <i class="fa fa-info-circle" aria-hidden="true"></i> For your records only | <i class="fa fa-times-circle" aria-hidden="true"> </i> Not Displayed in the frontend</small>
							</fieldset>

					</div>
				</div>


			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#projectText').APSummernote({
	 	placeholder : 'Hello',
	 });
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



