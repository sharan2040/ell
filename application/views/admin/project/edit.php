<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">




<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Projects</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">

				<div class="card">
					<div class="card-block">
						<form method="POST" action="<?php echo base_url()?>admin/projects/update" style="display:inline;">

							<input type="hidden" name="trackingId" value="<?=$projects[0]->tracking_id ?>">

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Unique ID</label>
								<input type="text" class="form-control" name="uniqueId" value="<?=$projects[0]->unique_id ?>" >
								<small class="text-help">This ID should not be changed Ideally | However you still have the provision to update it if necessary</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Customer ID</label>
								<input type="text" class="form-control" name="customerId" value="<?=$projects[0]->customer_id ?>">
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Customer Name</label>
								<input type="text" class="form-control" name="customerName" value="<?=$projects[0]->customer_name ?>" >
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Current Status</label>
								<textarea id="customerStatus" name="customerStatus"><?= $projects[0]->current_status;?></textarea>
								<small class="text-help">Current Status of the Project | <i class="fa fa-check-circle" aria-hidden="true"></i> Displayed to the customer in the Frontend</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Project Details</label>
								<textarea id="projectText" class="form-control" name="projectDetails"><?= $projects[0]->project_details;?></textarea>
								<small class="text-help">Details about the project | <i class="fa fa-info-circle" aria-hidden="true"></i> For your information only | <i class="fa fa-times-circle" aria-hidden="true"> </i> Not Displayed in the frontend</small>
							</fieldset>

							
 							<button type="submit" class="btn btn-primary">Update</button>
 						</form>
						<form method="POST" action="<?php echo base_url()?>admin/projects/delete" style="display:inline;">
							<input type="hidden" name="trackingId" value="<?=$projects[0]->tracking_id ?>">
 							<button type="submit" class="btn btn-danger">Delete</button>
 						</form>
					</div>
				</div>


			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#customerStatus').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



