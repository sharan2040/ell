      <!-- Page Title -->
      <section class="page-title">
        <div class="container">	  
         <div class="row">
          <div class="col-sm-12 col-md-12 title">
           <h2>Careers with us</h2>
           <ol class="breadcrumb">
            <li>You are here: &nbsp;</li>
            <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
            <li><a class="pathway" href="<?=base_url()?>about-us">About</a></li>
            <li class="active">Careers</li>				  
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Our Team -->
  <section class="main-body">
   <div class="container">
     <div class="row">
      <div class="col-md-12">
        <h3 align="center" style="color: #4b85a0;">Explore your opportunities in one of the UAE's fastest growing logistics companies.</h3>
        <h5 align="center">In order to meet our growth plans and future goals, we are always looking for talented individuals with a drive to perform in a challenging environment. We are uncompromising, always on the lookout for better. Whether you work at one of our global offices or onsite, a job at Elegant Line Logistics will be demanding. But there are also rewards for original thinking and hard work. And none of us here would have it any other way.</h5>
      </div>
    </div><br>
    <?php if( !empty($careers)): ?>

      <h4 align="right"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i> Open Positions</h4>
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <article itemtype="http://schema.org/Article" itemscope="" class="item item-page">
           <meta content="en-GB" itemprop="inLanguage">
           <div itemprop="articleBody" class="lists">
            <div class="panel-group lists-container" id="accordionTwo">

              <!-- Career Loop -->
              <?php foreach($careers as $career):?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordionTwo" href="#<?= $career->career_id?>" class="btn btn-block accord uk-text-left">
                        <div class="row">
                          <div class="col-md-4"><h5 class="accord-title" align="center"> <i class="fa fa-suitcase" aria-hidden="true"></i> <?= $career->job_title?></h5></div>
                          <div class="col-md-4"><h5 class="accord-title" align="center"> <i class="fa fa-calendar" aria-hidden="true"></i> End Date : <?= $career->application_enddate?> </h5></div>
                          <div class="col-md-4"><h5 class="accord-title" align="center"> <i class="fa fa-map-marker" aria-hidden="true"></i> Location : <?= $career->job_location?></h5></div>
                        </div>
                      </a> 
                    </div>
                  </div>
                  <div id="<?= $career->career_id?>" class="panel-collapse collapse">
                    <div class="panel-body">                         
                      <p><?= $career->job_description?></p>
                      <a class="readon" href="#"></a>
                    </div>
                  </div>
                </div>
              <?php endforeach;?>
              <!-- Career Loop ends here -->




            </div>  
          </div>
        </article>
      </div>
    </div>

  <?php else :?>
    <h3 align="center"><i class="fa fa-rss" aria-hidden="true"></i> There are no Openings Currently!</h3>
  <?php endif;?>
</div>
</section>
<!-- /Our Team -->

<!-- Get A Quote -->
<section class="parallax-section-2">
 <div class="row">
  <div class="col-sm-12 col-md-12" id="parallax2">
    <div class="module ">
     <div class="module-content">
       <div class="uk-text-contrast uk-flex uk-flex-center" data-uk-parallax="{bg: -300}" style="min-height: 160px; background-color: rgb(41, 44, 47); background-repeat: no-repeat; background-size: cover; background-position: 50% -42.435px;">
        <div class="container1170" style="padding:60px 15px;">
          <div class="uk-grid">
              <h4 style="margin-top:15px;" align="center">If you think you are the right Candidate, send us your updated resume to careers@elegantlinelogistics.com with Job Title as subject.</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>	  
<!-- /Get A Quote -->			  
