      <!-- Page Title -->
      <section class="page-title">
        <div class="container">   
          <div class="row">
            <div class="col-sm-12 col-md-12 title">
              <h2>Cargo Lashing Services</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
                <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
                <li><a class="pathway" href="#">Services</a></li>
                <li class="active">Cargo Lashing Services</li>          
              </ol>
            </div>
          </div>
        </div>
      </section>
      <!-- /Page Title -->

      <!-- Top A -->
      <section class="main-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="module title3">
              <div class="module-content">
                  <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-2-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/cargo-lashing.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/cargo-lashing-1.jpg" alt="Luxury Residential Building">
                      <hr>
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/cargo-lashing-2.jpg" alt="Luxury Residential Building">
                      <hr>
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/cargo-lashing-3.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <strong>We offer cargo lashing products at an affordable price, and they are durable and long lasting in nature..</strong><hr>
                      <p>At ELL we use products such as chord straps, nylon straps, ratchetbelts that helps choking the movement of the cargo in the container. We specialise in :</p>
                      <ul>
                        <li>- Container Lashing Services</li>
                        <li>- General Cargo Lashing Services</li>
                        <li>- Pipe Lashing Services</li>
                        <li>- Car Lashing Services</li>
                        <li>- Equipment Machinery Lashing Services</li>
                        <li>- Rig Packing Services</li>
                        <li>- Project Cargo Lashing Services</li>
                        <li>- Seaport / Onboard Lashing Services</li>
                      </ul>
                      
                    </div>
                  </div>
                  <br>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>    
    <!-- /Top A -->