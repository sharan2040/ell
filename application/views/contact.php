<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
       	"use strict";
       	var myCenter=new google.maps.LatLng('<?= $settings[0]->latitude ?>', '<?= $settings[0]->longitude ?>');
       	function initialize() {
       		var mapCanvas = document.getElementById('mapCanvas');
       		var mapOptions = {
       			center: myCenter,
       			zoom: 16,
       			scrollwheel: false,
       			mapTypeId: google.maps.MapTypeId.ROADMAP
       		}
       		var map = new google.maps.Map(mapCanvas, mapOptions)
        //var iconBase = 'https://maps.google.com/mapfiles/kml/pushpin/';
        var iconBase = 'images/elements/';
        var marker = new google.maps.Marker({
        	position: mapOptions.center,
        	map: map,
        	icon: iconBase + 'helmet.png'
        });	
        marker.setMap(map);
        var infowindow = new google.maps.InfoWindow({
        	content:"<div id='legend' style='color: #000 !important; text-align: center;'>Our Corporate Office!</div>"
        });
        infowindow.open(map,marker);		
    }
    google.maps.event.addDomListener(window, 'load', initialize);  


</script>	

<script>
$(document).ready(function()
{
	// process the form
    $('#contactForm').submit(function(event) {

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '<?= base_url()."staticpages/contactSendMail";?>', // the url where we want to POST
            data        : $(this).serialize(), // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode         : true
        })
            // using the done promise callback
            .done(function(data) {
            	alert(Hello);
                // log data to the console so we can see
                console.log(data); 

                // here we will handle errors and validation messages
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
	   	$( "#contactForm" ).fadeOut( "slow", function() {
	    	$('#messageDiv').html("<h3 align='center' style='color:green; padding-top:200px;'> <i class='fa fa-check-circle-o' aria-hidden='true'></i> Thanks for contacting us<br> We will get back to you soon!!</h3>");
	  	});


    });

  
});
</script>


<!-- Page Title -->
<section class="page-title">
	<div class="container">	  
		<div class="row">
			<div class="col-sm-12 col-md-12 title">
				<h2>Contact Us</h2>
				<ol class="breadcrumb">
					<li>You are here: &nbsp;</li>
					<li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
					<li class="active">Contact Us</li>				  
				</ol>
			</div>
		</div>
	</div>
</section>
<!-- /Page Title -->

<!-- Contact Us -->
<section class="main-body">
	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-3" id="left">
				<div class="module title6">
					<div class="module-content">
						<h4><strong>Let's build something together!</strong></h4>
						<hr>
						<i class="pe-7s-pin pe-3x uk-text-primary"></i> <h4><?= $settings[0]->company_address ?></h4>
						<hr>
						<i class="pe-7s-phone pe-3x uk-text-primary"></i> <h4><?= $settings[0]->contact_phone ?></h4>
						<hr>
						<i class="pe-7s-mail pe-3x uk-text-primary"></i> <h5><?= $settings[0]->contact_email ?></h5>
						<hr>
					</div>
				</div>
			</div>
			<div class="col-sm-9 col-md-9" id="messageDiv">
				<h3>Contact</h3>					  
				<form class="form-horizontal" id="contactForm" method="post" action="staticpages/sendMail">
					<fieldset>
						<legend>Send an Email. All fields with an asterisk (*) are required.</legend>					
						<div class="form-group">
							<label for="inputName" class="control-label label-left col-xs-3">Name *</label>
							<div class="col-xs-9">
								<input type="text" class="form-control" id="inputName" placeholder=" Your name" name="inputName" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="control-label label-left col-xs-3">Email *</label>
							<div class="col-xs-9">
								<input type="email" class="form-control" id="email" placeholder=" Email for contact" name="email" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="inputSubject" class="control-label label-left col-xs-3">Subject *</label>
							<div class="col-xs-9">
								<input type="text" class="form-control" id="inputSubject" placeholder=" Enter the subject of your message here" name="inputSubject" required="">
							</div>
						</div>
						<div class="form-group">
							<label for="inputMessage" class="control-label label-left col-xs-3">Message *</label>
							<div class="col-xs-9">
								<textarea class="form-control" id="inputMessage" placeholder="Enter your message here" name="inputMessage" required=""></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="sendToYourself" class="control-label label-left col-xs-3">Send copy to yourself</label>
							<div class="col-xs-9">
								<div class="checkbox">
									<label><input type="checkbox" id="sendToYourself" name="sendToYourself">Sends a copy of the message to the address you have supplied</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label label-left col-xs-3"></label>			
							<div class="col-xs-9">
								<button class="btn btn-primary validate" type="submit" aria-invalid="false">Send Email</button>
							</div>
						</div>
					</fieldset>				
				</form>					
			</div>
		</div>
	</div>
</section>
<!-- /Contact Us -->


<!-- Map -->	
<section class="map">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div id="mapCanvas" class="map-canvas"></div>		  
			</div>		  
		</div>
	</div>
</section>	  
<!-- /Map -->		  

