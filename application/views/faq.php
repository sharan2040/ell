      <!-- Page Title -->
      <section class="page-title">
        <div class="container">	  
         <div class="row">
          <div class="col-sm-12 col-md-12 title">
           <h2>Frequently Asked Questions</h2>
           <ol class="breadcrumb">
            <li>You are here: &nbsp;</li>
            <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
            <li><a class="pathway" href="<?php echo base_url();?>about-us">About</a></li>
            <li class="active">FAQ</li>				  
          </ol>
        </div>
      </div>
    </div>
  </section>
  <!-- /Page Title -->

  <!-- Our Team -->
  <section class="main-body">
   <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <article itemtype="http://schema.org/Article" itemscope="" class="item item-page">
         <meta content="en-GB" itemprop="inLanguage">
         <div itemprop="articleBody" class="lists">
          <div class="panel-group lists-container" id="accordionTwo">

          <?php foreach($faqs as $faq): ?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordionTwo" href="#collapse<?= $faq->faq_id ?>" class="btn btn-block accord uk-text-left"><h4 class="accord-title"><?= $faq->question ?></h4></a>	
                </div>
              </div>
              <div id="collapse<?= $faq->faq_id ?>" class="panel-collapse collapse">
                <div class="panel-body">                         
                  <p><?= $faq->answer ?></p>
                  <a class="readon" href="#"></a>
                </div>
              </div>
            </div>
          <?php endforeach; ?>

          </div>	
        </div>
      </article>
    </div>
  </div>
</div>
</section>
<!-- /Our Team -->

<!-- Get A Quote -->
<section class="parallax-section-2">
 <div class="row">
  <div class="col-sm-12 col-md-12" id="parallax2">
    <div class="module ">
     <div class="module-content">
       <div class="uk-text-contrast uk-flex uk-flex-center" data-uk-parallax="{bg: -300}" style="min-height: 160px; background-color: rgb(41, 44, 47); background-repeat: no-repeat; background-size: cover; background-position: 50% -42.435px;">
        <div class="container1170" style="padding:60px 15px;">
          <div class="uk-grid">
            <div class="uk-width-medium-3-4">
              <h3 style="margin-top:15px;">Do you want the best in the business to work for you?</h3>
            </div>
            <div class="uk-width-medium-1-4 uk-text-right inverted">
              <h3 style="margin-top:5px;"><a class="uk-button" href="<?= base_url()?>get-a-quote">Get A Quote Now</a></h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>	  
<!-- /Get A Quote -->			  
