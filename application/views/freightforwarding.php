      <!-- Page Title -->
      <section class="page-title">
        <div class="container">   
          <div class="row">
            <div class="col-sm-12 col-md-12 title">
            <h2>Freight Forwarding</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
                <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
                <li><a class="pathway" href="#">Services</a></li>
                <li class="active">Freight Forwarding</li>          
              </ol>
            </div>
          </div>
        </div>
      </section>
      <!-- /Page Title -->

      <!-- Top A -->
      <section class="main-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="module title3">
                <div class="module-content">
                  <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-2-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/freight-forwarding.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/freight-forwarding-3.jpg" alt="Luxury Residential Building">
                      <hr>
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/freight-forwarding-2.jpg" alt="Luxury Residential Building">
                      <hr>
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/freight-forwarding-1.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <p>We rank amongst UAE's most versatile logistics solutions companies, adhering to stipulated compliances and accreditations such as MTO, IATA, FIATA. <br>
                      <strong>ROAD FREIGHT :</strong> We specialise in offering road freight services, providing unique expertise in the consolidation and movement of freight throughout UAE.<br>

                        <strong>SEA FREIGHT :</strong> Sea freight is the most economical and long established way of sending large volumes of goods internationally.<br>

                        <strong>AIR FREIGHT :</strong> Air transportation is advised for executive relocations, national and international travellers, students and all those in search of urgent shipments. </p>
                      </div>
                    </div>
                    <br>
                    <p class="blockquote-pc">Our strategic association with international freight forwarders and customs brokerages allows our clients to avail a wide range of state-of-the-art, tailor-made freight services in a timely and cost effective manner!</p>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>    
    <!-- /Top A -->