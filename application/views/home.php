

      <!-- Slider -->
      <section id="ukSlider" class="uk-slider-section">
        <div class="container-fluid">
         <div class="row">                        
          <div class="col-md-12 padding-0">		
            <div class="uk-slidenav-position" data-uk-slideshow="{animation: 'fade', autoplayInterval:'4000', autoplay:true}">
              <ul class="uk-slideshow uk-overlay-active" style="height: 560px;">				
                <li aria-hidden="false" class="uk-active" style="animation-duration: 500ms; width: 100%; height: auto;">
                  <img width="1920" height="560" alt="" src="<?=base_url()?>assets/default/images/elements/sl0.jpg">
              <!--     <div class="uk-overlay-panel uk-overlay-fade">
                    <div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">
                     <div class="uk-width-1-10">
                     </div>
                     <div class="uk-width-4-10 uk-text-left">
                      <div class="wow fadeInLeft slider-text slider-text-2" data-wow-delay="0.7s" >
                       <h1 class="raleway80 uk-text-white">WE BUILD</h1>
                     </div>
                     <div class="wow fadeInLeft slider-text slider-text-2" data-wow-delay="1.2s">
                       <h2 class="raleway80 uk-slider-text-white">DREAMS</h2>						
                     </div>
                     <br class="uk-hidden-small">						  
                     <div class="wow rollIn slider-text  slider-text-2" data-wow-delay="1.7s">
                      <a href="get-a-quote.html" class="uk-button raleway16">Get A Quote</a>
                    </div>												
                  </div>
                  <div class="uk-width-1-10">
                  </div>						
                  		
                </div>
              </div> -->
            </li>

           <li aria-hidden="true" class="" style="animation-duration: 500ms; width: 100%; height: auto;">
            <img width="1920" height="700" alt="" src="<?=base_url()?>assets/default/images/elements/slider-local1.jpg">
            <!-- <div class="uk-overlay-panel uk-overlay-fade">
              <div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">  
               <div class="uk-width-5-10">
               </div>           
                <div class="wow fadeInUp uk-width-5-10"  data-wow-delay="4.2s">
                  <img width="530" height="100" alt="Sign" src="<?=base_url()?>assets/default/images/elements/sign.png" class="img-responsive col-xs-12">
                </div>
             
             </div>
           </div> -->
         </li>    

         <li aria-hidden="true" class="" style="animation-duration: 500ms; width: 100%; height: auto;">
          <img width="1920" height="700" alt="" src="<?=base_url()?>assets/default/images/elements/slider-office.jpg">
         <!-- <div class="uk-overlay-panel uk-overlay-fade">
              <div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">  
               <div class="uk-width-5-10">
               </div>           
                <div class="wow fadeIn uk-width-5-10"  data-wow-delay="4.2s">
                  <img width="530" height="100" alt="Sign" src="<?=base_url()?>assets/default/images/elements/office-1.png" class="img-responsive col-xs-12">
                </div>
             
             </div>
           </div> -->
       </li>    

           <li aria-hidden="true" class="" style="animation-duration: 500ms; width: 100%; height: auto;">
            <img width="1920" height="700" alt="" src="<?=base_url()?>assets/default/images/elements/sl-international-relocation1.jpg">
            <!-- <div class="uk-overlay-panel uk-overlay-fade">
              <div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">	
               <div class="uk-width-5-10">
               </div> 					
                <div class="wow fadeInUp uk-width-5-10"  data-wow-delay="4.2s">
                  <img width="530" height="100" alt="Sign" src="<?=base_url()?>assets/default/images/elements/sign.png" class="img-responsive col-xs-12">
                </div>
             
             </div>
           </div> -->
         </li>			
         

        <li aria-hidden="true" class="" style="animation-duration: 500ms; width: 100%; height: auto;">
            <img width="1920" height="700" alt="" src="<?=base_url()?>assets/default/images/elements/sl-freight-forwarding.jpg">
            <!-- <div class="uk-overlay-panel uk-overlay-fade">
              <div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">  
               <div class="uk-width-5-10">
               </div>           
                <div class="wow fadeInUp uk-width-5-10"  data-wow-delay="4.2s">
                  <img width="530" height="100" alt="Sign" src="<?=base_url()?>assets/default/images/elements/sign.png" class="img-responsive col-xs-12">
                </div>
             
             </div>
           </div> -->
         </li>    

          <li aria-hidden="true" class="" style="animation-duration: 500ms; width: 100%; height: auto;">
            <img width="1920" height="700" alt="" src="<?=base_url()?>assets/default/images/elements/sl-warehouses.jpg">
            <!-- <div class="uk-overlay-panel uk-overlay-fade">
              <div class="uk-container uk-container-center uk-height-1-1 uk-width-1-1 uk-flex uk-flex-middle">  
               <div class="uk-width-5-10">
               </div>           
                <div class="wow fadeInUp uk-width-5-10"  data-wow-delay="4.2s">
                  <img width="530" height="100" alt="Sign" src="<?=base_url()?>assets/default/images/elements/sign.png" class="img-responsive col-xs-12">
                </div>
             
             </div>
           </div> -->
         </li>    
          	  
     </ul>
     <a data-uk-slideshow-item="previous" class="uk-slidenav  uk-slidenav-previous uk-hidden-touch" href="#"></a>
     <a data-uk-slideshow-item="next" class="uk-slidenav  uk-slidenav-next uk-hidden-touch" href="#"></a>				
   </div>
 </div>
</div>
</div>
</section>
<!-- /Slider -->

<!-- Page Title -->
<section class="page-title" style="background-image:url("../../images/home2.png")">
  <div class="container">   
   <div class="row">
    <div class="col-sm-12 col-md-12 title">
     <h3 style="display: inline; font-size: 20px;"><i class="fa fa-map-marker" aria-hidden="true"></i> ACROSS THE STREET, ACROSS THE WORLD. THE BEST MOVES ARE MADE WITH ELL</h3>
      <h3>Let us take away the stress that comes with moving, as our expert team is here to cater to all of your needs. From packing to transit and delivery, our professionals provide a high level of customer service and support to ensure your move is cost-effective, efficient and secure.</h3>                  
  </div>
</div>
</div>
</section>
<!-- /Page Title -->


<!-- Top A -->
<section class="top-a">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
       <div class="module title1">
        <div class="module-content">
          <div class="sprocket-strips">
           <ul data-strips-items="" class="sprocket-strips-container cols-3">
            <li>
             <div style="background-image: url(images/elements/service1.jpg);" class="sprocket-strips-item">
              <div class="sprocket-strips-content">
                <h4 class="sprocket-strips-title">
                  <a href="<?php echo base_url();?>international-removal">International Relocation</a>
                </h4>
                <span class="sprocket-strips-text">Moving Door-to-door, to nearly anywhere in the world, for past 6 Years</span>
                <a class="readon" href="<?php echo base_url();?>international-removal"></a>
              </div>
            </div>
          </li>
          <li>
           <div style="background-image: url(images/elements/service2.jpg);" class="sprocket-strips-item">
            <div class="sprocket-strips-content">
              <h4 class="sprocket-strips-title">
                <a href="<?php echo base_url();?>local-removal">Domestic Relocation</a>
              </h4>
              <span class="sprocket-strips-text">Looking to shift around UAE? We will handle the hassles for you</span>
              <a class="readon" href="<?php echo base_url();?>local-removal"></a>
            </div>
          </div>
        </li>
        <li>
         <div style="background-image: url(images/elements/service3.jpg);" class="sprocket-strips-item">
          <div class="sprocket-strips-content">
            <h4 class="sprocket-strips-title">
              <a href="<?php echo base_url();?>office-removal">Office Relocation</a>
            </h4>
            <span class="sprocket-strips-text">Relocate your Office belongings at the new premises with minimal down time</span>
            <a class="readon" href="<?php echo base_url();?>office-removal"></a>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
</div>
</div>
</div>
<div class="row">
  <div class="col-sm-12 col-md-12">
    <div class="module title3">
      <div class="module-content">

        <hr class="uk-grid-divider">
        <div class="uk-grid uk-grid-divider">
          <div class="uk-width-medium-1-3">
            <h4>
              <i class="pe-7s-global pe-2x pe-va"></i>&nbsp;&nbsp;&nbsp;<strong>Global Shipping Partners</strong>
            </h4>
            <p>A reach that comes from eight international licensees and relationships with more than 60 partners around the world. We've been in the business of international relocation for over 4 years..</p>
          </div>
          <div class="uk-width-medium-1-3">
            <h4>
              <i class="pe-7s-graph1 pe-2x pe-va"></i>&nbsp;&nbsp;&nbsp;<strong>Experienced Team Leaders</strong>
            </h4>
            <p>We know the regulations, requirements and the specifics of moving. ELL make thousands of moves per year, giving us the expertise that's required to assure that your goods arrive safely and securely.</p>
          </div>
          <div class="uk-width-medium-1-3">
            <h4>
              <i class="pe-7s-next pe-2x pe-va"></i>&nbsp;&nbsp;&nbsp;<strong>6 Years in the Market</strong>
            </h4>
            <p>Over the years, we have demonstrated to our customers not just our capabilities, but also our values: integrity, pragmatism, discretion, and service-mindedness..</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>    
<!-- /Top A -->

<!-- Construction Process -->	  
<section class="top-b">
 <div class="container">
  <div class="row">
    <div class="col-sm-12 col-md-12">
     <div class="module title1">
      <h3 class="module-title">Our Modus Operandi</h3>
      <div class="module-content">
       <div class="uk-grid uk-grid-divider-2">
        <div data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:100}" class="uk-width-medium-1-6 uk-scrollspy-init-inview uk-scrollspy-inview">
          <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1b">
            <span class="hi-icon pe-7s-note"></span><h4>Home Survey</h4>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
          </div>
        </div>
        <div data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:300}" class="uk-width-medium-1-6 uk-scrollspy-init-inview uk-scrollspy-inview">
          <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1b">
            <span class="hi-icon pe-7s-calculator"></span><h4>Cost Estimation</h4>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
          </div>
        </div>
        <div data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:500}" class="uk-width-medium-1-6 uk-scrollspy-init-inview uk-scrollspy-inview">
          <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1b">
            <span class="hi-icon pe-7s-note2"></span><h4>Quote Confirmation</h4>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
          </div>
        </div>
        <div data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:700}" class="uk-width-medium-1-6 uk-scrollspy-init-inview uk-scrollspy-inview">
          <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1b">
            <span class="hi-icon pe-7s-box2"></span><h4>Packing Goods</h4>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
          </div>
        </div>
        <div data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:900}" class="uk-width-medium-1-6 uk-scrollspy-init-inview uk-scrollspy-inview">
          <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1b">
            <span class="hi-icon pe-7s-plane"></span><h4>Moving Around</h4>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
          </div>
        </div>
        <div data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:1100}" class="uk-width-medium-1-6 uk-scrollspy-init-inview uk-scrollspy-inview">
          <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1b">
            <span class="hi-icon pe-7s-home"></span><h4>Delivery/Installation</h4>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</section>
<!-- /Construction Process -->


<!-- Latest Construction News -->   
<section class="main-body position-a">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="module title1">
          <h3 class="module-title">Latest News &amp; Events</h3>
          <div class="module-content sprocket-strips-s">
          <?php if($blogs): ?>
            <div data-uk-slideset="{small: 1, medium: 2, large: 3}">
              <ul class="uk-grid uk-slideset"> 
                
               
                <?php foreach($blogs as $blog):?>
                <li class="sprocket-strips-s-block" style="opacity: 1;">
                  <div class="sprocket-strips-s-item">
                    <img alt="" src="<?=base_url()?>assets/uploads/<?= $blog->cover_image?>">
                    <br>
                    <div class="sprocket-strips-s-content">
                      <h4 class="sprocket-strips-s-title">
                        <a href="news/news5.html"><?= $blog->blog_title?></a>
                      </h4>
                      <span class="date"><?php echo date('d M Y', strtotime($blog->created_date))?></span>
                      <br>
                      <span class="sprocket-strips-s-text"><?= substr(strip_tags($blog->contents), 0, 150);?>...</span>
                      <br><br><a class="readon" href="<?= base_url().'blog/'.$blog->slug;?>"><span>Read More</span></a>
                    </div>
                  </div>
                </li>
                <?php endforeach;?>
              </ul>
              <div class="uk-grid">
                <div class="uk-width-small-1-5 uk-push-2-5 uk-flex uk-flex-center" >
                  <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>             
                  <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>             
                </div>                                
              </div>          
            </div>
          <?php else: ?>
            <h5 align="center"><i class="fa fa-retweet" aria-hidden="true"></i> Oops! No Recent Blogs</h5>
          <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
    <!-- /Latest Construction News -->