      <!-- Page Title -->
      <section class="page-title">
    <div class="container">   
        <div class="row">
        <div class="col-sm-12 col-md-12 title">
        <h2>Industrial Packing</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
        <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
        <li><a class="pathway" href="#">Services</a></li>
        <li class="active">Industrial Packing</li>          
          </ol>
      </div>
      </div>
    </div>
    </section>
    <!-- /Page Title -->
    
    <!-- Top A -->
      <section class="main-body">
      <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12">
        <div class="module title3">
        <div class="module-content">
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-2-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/industrial-packing.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/industrial-packing-1.jpg" alt="Luxury Residential Building">
                      <hr>
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/industrial-packing-2.jpg" alt="Luxury Residential Building">
                      <hr>
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/industrial-packing-3.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <p>If you're looking for effective packaging solutions for commercial or industrial purposes, Elegant Line Logistics is sure to find the right ones for you.We render the best quality of Industrial Packaging Services owing to our rich experience and  skilled proffessionals  Known for their timely execution. The rendered industrial packaging services can be customized as per the specifications provided by the patrons.</p>
                      <hr>
                      <strong>Our team takes precautions to make sure that the precious goods of our customers are not damaged during transportation process.</strong>
                    </div>
                  </div>
                  <br>
                </div>
        </div>
      </div>
      </div>
    </div>
    </section>    
    <!-- /Top A -->