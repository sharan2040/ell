      <!-- Page Title -->
      <section class="page-title">
        <div class="container">   
          <div class="row">
            <div class="col-sm-12 col-md-12 title">
              <h2>International Household goods &amp; Personal effects Relocation</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
                <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
                <li><a class="pathway" href="#">Services</a></li>
                <li class="active">International Removal</li>          
              </ol>
            </div>
          </div>
        </div>
      </section>
      <!-- /Page Title -->

      <!-- Top A -->
      <section class="main-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="module title3">
                <div class="module-content">
                  <p style="font-size: 15px; color: #1F4161; margin-bottom: 15px;">Moving to a new country can be overwhelming, so we want to make your transition as easy as possible. Our international removals consultants have the proven ability to manage relocations from beginning to end, and are able to address any questions, concerns or requirements throughout the entire process. We provide tracking and status reports throughout your overseas move. With ELL, you’ll know where your belongings are at all phases of the move and when you can expect them to arrive.. For further information on office relocations, <a href="<?=base_url()?>contact"><strong>contact us</strong></a> today.</p>
                  <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-2-4">
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/international-relocation.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/international-relocation-1.jpg" alt="Luxury Residential Building">
                      <hr>
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/international-relocation-2.jpg" alt="Luxury Residential Building">
                      <hr>
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/international-relocation-3.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <p><strong>MOVING OVERSEAS :</strong> We know that to provide an impeccable international removal service, we need to be more than just a logistic company. That’s why we get to know our clients’ goals and ambitions, just as well as we know our own. We recruit the best staff in the international relocation industry, and train them to excel.<br>

                        <strong>CUSTOMER SERVICE :</strong> In our business, listening goes far beyond being polite and professional. It's quite simply the most important tool we have in designing and implementing your company's global mobility programme.<br>

                        <strong> NO CONCERN IS SMALL, NO REQUEST IS TOO BIG:</strong> If you have a concern, we adopt that concern as our own and work toward a solution that's right for you.<br>

                      </p>
                    </div>
                  </div>
                  <br>
                </div>
                <hr>
              </div>

            </div>
          </div>
        </div>
      </section>    
    <!-- /Top A -->