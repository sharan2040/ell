      <!-- Page Title -->
      <section class="page-title">
        <div class="container">	  
         <div class="row">
          <div class="col-sm-12 col-md-12 title">
           <h2>Domestic Household goods &amp; Personal effects Relocation</h2>
           <ol class="breadcrumb">
            <li>You are here: &nbsp;</li>
            <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
            <li><a class="pathway" href="#">Services</a></li>
            <li class="active">Local Removal</li>				  
          </ol>
        </div>
      </div>
    </div>
  </section>
  <!-- /Page Title -->

  <!-- Top A -->
  <section class="top-a">
   <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
       <div class="module title3">
         <div class="module-content">
           <p style="font-size: 15px; color: #1F4161; margin-bottom: 15px;">Moving locally is a stressful experience for many. From packing and loading to unloading, organising and cleaning, the whole process can leave you feeling physically and mentally drained. At ELL, our team is committed to make your move as stress-free as possible. When you move with us, you will be appointed with your own dedicated moving consultant, who will be there to answer any questions and address any concerns you might have... For further information on office relocations, <a href="<?=base_url()?>contact"><strong>contact us</strong></a> today.</p>
           <div class="uk-grid" data-uk-grid-margin="">
            <div class="uk-width-medium-2-4">
              <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/home-relocation.jpg" alt="Luxury Residential Building">
            </div>
            <div class="uk-width-medium-1-4">
              <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/home-relocation-2.jpg" alt="Luxury Residential Building">
              <hr>
              <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/home-relocation-3.jpg" alt="Luxury Residential Building">
              <hr>
              <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/home-relocation-1.jpg" alt="Luxury Residential Building">
            </div>
            <div class="uk-width-medium-1-4">
              <p><strong>HOME SURVEY :</strong> We provide a free survey of your home and contents to ensure we fully understand your requirements in detail.<br>

                <strong>QUOTATION :</strong> Following the survey we will provide you with a professional and competitive quote based on your needs for you to consider in your own time.<br>

                <strong>PACKING & LOADING: </strong> We take great care in training our packing crews in the latest packing techniques.We also pay close attention to the quality of our packing materials and take just as much care when unloading and unpacking.<br>

                <strong>STORAGE :</strong> If you won't be moving into your new home immediately, we can arrange storage facilities and deliver them to you as per your convenience. </p>
              </p>
            </div>
          </div>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>
</section>	  
<!-- /Top A -->

<!-- Get A Quote -->
<section class="parallax-section-2">
 <div class="row">
  <div class="col-sm-12 col-md-12" id="parallax2">
    <div class="module ">
     <div class="module-content">
       <div class="uk-text-contrast uk-flex uk-flex-center" data-uk-parallax="{bg: -300}" style="min-height: 160px; background-color: rgb(41, 44, 47); background-repeat: no-repeat; background-size: cover; background-position: 50% -42.435px;">
        <div class="container1170" style="padding:60px 15px;">
          <div class="uk-grid">
            <div class="uk-width-medium-3-4">
              <h3 style="margin-top:15px;">Do you want the best in the business to work for you?</h3>
            </div>
            <div class="uk-width-medium-1-4 uk-text-right inverted">
              <h3 style="margin-top:5px;"><a class="uk-button" href="<?php echo base_url();?>get-a-quote">Get A Quote Now</a></h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>    
<!-- /Get A Quote -->       