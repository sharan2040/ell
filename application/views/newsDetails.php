     <?php if(!empty($blogDetails)): ?>
     	<!-- Page Title -->
     	<section class="page-title">
     		<div class="container">	  
     			<div class="row">
     				<div class="col-sm-12 col-md-12 title">
     					<h2><?= $blogDetails[0]->blog_title;?></h2>
     					<ol class="breadcrumb">
     						<li>You are here: &nbsp;</li>
     						<li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
     						<li><a class="pathway" href="<?php echo base_url();?>news">Blog</a></li>				
     						<li class="active"><?= $blogDetails[0]->blog_title;?></li>				  
     					</ol>
     				</div>
     			</div>
     		</div>
     	</section>
     	<!-- /Page Title -->
     <?php else :?>
     	<section class="page-title">
     		<div class="container">	  
     			<div class="row">
     				<div class="col-sm-12 col-md-12 title">
     					<h2>Invalid Blog</h2>
     					<ol class="breadcrumb">
     						<li>You are here: &nbsp;</li>
     						<li><a class="pathway" href="../index.html">Home</a></li>
     						<li><a class="pathway" href="../blog.html">Blog</a></li>				
     						<li class="active"></li>				  
     					</ol>
     				</div>
     			</div>
     		</div>
     	</section>
     <?php endif;?>


     <!-- Blog -->
     <section class="main-body">
     	<div class="container">
     		<div class="row">
     			<?php if(!empty($blogDetails)): ?>
     				<div class="col-sm-9 col-md-9 left">
     					<div itemtype="http://schema.org/Blog" itemscope="" class="blog">
     						<div class="row clearfix">
     							<div class="col-sm-12">
     								<article class="item item-page" itemtype="http://schema.org/Article" itemscope="">
     									<div>
     										<dl class="article-info">
     											<dt class="article-info-term"></dt>				
     											<dd class="category-name">
     												<i class="fa fa-folder-open-o"></i>
     												<a title="" data-toggle="tooltip" itemprop="genre" href="../blog.html" data-original-title="Article Category">Blog</a>
     											</dd>			
     											<dd class="published">
     												<i class="fa fa-calendar-o"></i>
     												<time title="" data-toggle="tooltip" itemprop="datePublished" datetime="2015-11-10T08:14:20+00:00" data-original-title="Published Date">
     													<?php echo date('d M Y', strtotime($blogDetails[0]->created_date))?>
     												</time>
     											</dd>			
     											<dd class="post_rating">
     												Views : <?= $blogDetails[0]->views;?>
     											</dd>
     										</dl>
     										<h2 itemprop="name" style="font-size: 24px; margin-bottom: 30px;">
     											<?= $blogDetails[0]->blog_title;?>
     										</h2>
     									</div>
     									<p><img src="<?= base_url().'assets/uploads/'.$blogDetails[0]->cover_image?>" width="100%" alt="Construction sites review"></p>
     									<?= $blogDetails[0]->contents;?>
     									<hr>
     								</article>
     								<!-- end item -->
     							</div><!-- end col-sm-* -->
     						</div><!-- end row -->
     						
     					</div>
     				</div>
     			<?php else :?>
     				<div class="col-sm-9 col-md-9 left">
     					<h3 align="center" style="padding-top:60px;"><i class="fa fa-frown-o" aria-hidden="true"></i> No such Blog</h3>
     					<h4 align="center">Maybe you followed a wrong/broken link</h4>
     				</div>


     			<?php endif;?>

     			<div class="col-sm-3 col-md-3 right">
     				<div class="module _menu">
     					<h3 class="module-title">Recent News</h3>
     					<div class="module-content">
     						<ul class="nav menu">
     							<?php if(!empty($allBlogs)):?>
     								<?php foreach($allBlogs as $blog):?>
     									<li><a href="<?= base_url().'blog/'.$blog->slug;?>"><?= substr($blog->blog_title, 0, 35);?>...</a></li>
     								<?php endforeach; ?>
     							<?php else : ?>
     								<li><a href="#">No Blogs Recently</a></li>
     							<?php endif;?>	
     						</ul>
     					</div>
     				</div>
     			</div>  
     		</div>
     	</div>
     </section>
     <!-- /Blog -->
