      <!-- Page Title -->
      <section class="page-title">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>News &amp; Events</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
				<li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
				<li class="active">News &amp; Events</li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Blog -->
      <section class="main-body">
	    <div class="container">
          <div class="row">
          	<?php if($blogs) :?>
		    <div class="col-sm-1 col-md-12">

              <div itemtype="http://schema.org/Blog" itemscope="" class="blog">
				<div class="row clearfix">

				<?php foreach($blogs as $blog):?>
				  <div class="col-sm-6">
				    <article itemtype="http://schema.org/BlogPosting" itemscope="" itemprop="blogPost" class="item ">
					  <div class="entry-header has-post-format">
          	            <span class="post-format"><i class="fa fa-thumb-tack"></i></span>
                        <dl class="article-info">
			              <dt class="article-info-term"></dt>				
					      <dd class="category-name">
	                        <i class="fa fa-folder-open-o"></i>
				            <a title="" data-toggle="tooltip" itemprop="genre" href="blog.html" data-original-title="Article Category">Blog</a>
						  </dd>			
						  <dd class="published">
	                        <i class="fa fa-calendar-o"></i>
	                        <time title="" data-toggle="tooltip" itemprop="datePublished" datetime="2015-11-10T08:14:20+00:00" data-original-title="Published Date">
		                      <!-- 10 November 2015 -->
		                      <?php echo date('d M Y', strtotime($blog->created_date))?>
							</time>
                          </dd>			
					      <dd class="post_rating">
		                    Rating: 
							<div class="voting-symbol">
			                  <span data-number="5" class="star active"></span><span data-number="4" class="star active"></span><span data-number="3" class="star active"></span><span data-number="2" class="star active"></span><span data-number="1" class="star active"></span>
							</div>
                          </dd>
	                    </dl>
					    <h2 itemprop="name">
					      <a itemprop="url" href="<?= base_url().'blog/'.$blog->slug;?>"><?=$blog->blog_title ?></a>
					    </h2>
					  </div>
                      <p><img src="<?=base_url()?>assets/uploads/<?=$blog->cover_image ?>" alt="Welding future?"></p>
                      <p><?= substr(strip_tags($blog->contents), 0, 300);?>...</p>
                      <p class="readmore">
	                    <a itemprop="url" href="<?= base_url().'blog/'.$blog->slug;?>" class="btn btn-default">Read more ...	</a>
                      </p>
                      <hr>
				    </article>
				    <!-- end item -->
				  </div><!-- end col-sm-* -->
				<?php endforeach;?>
			<?php else :?>
				<br>
				<h3 align="center"><i class="fa fa-frown-o" aria-hidden="true"></i> Oh Snap! No News or Events</h3>
				<h4 align="center">We shall update soon</h4>
				<br>
			<?php endif; ?>

			
				
				</div><!-- end row -->
				<!-- <div class="pagination-wrapper">
				  <p class="counter"> Page 1 of 2 </p>
				  <ul class="pagination">
					<li class="active"><a>1</a></li>
					<li><a title="2" href="#" class="">2</a></li>
					<li><a title="»" href="#" class="next">»</a></li>
					<li><a title="End" href="#" class="">End</a></li>
			      </ul>
				</div> -->
	          </div>
            </div>
		   
		  </div>
	    </div>
	  </section>
	  <!-- /Blog -->

