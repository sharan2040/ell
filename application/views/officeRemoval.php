      <!-- Page Title -->
      <section class="page-title">
        <div class="container">   
          <div class="row">
            <div class="col-sm-12 col-md-12 title">
              <h2>Office Relocation &amp; Furniture Configuration</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
                <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
                <li><a class="pathway" href="#">Services</a></li>
                <li class="active">Office Relocation</li>          
              </ol>
            </div>
          </div>
        </div>
      </section>
      <!-- /Page Title -->

      <!-- Top A -->
      <section class="main-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="module title3">
                <div class="module-content">
                  <p style="font-size: 15px; color: #1F4161; margin-bottom: 15px;">We have a wide choice of office and commercial relocation options available. With multiple branch locations across UAE, and an extensive global partner network, our clients can confidently rely on our services to deliver on time. Whether you’re moving from floor-to-floor, building-to-building, or state-to-state, you choose what option fits your needs and our project managers will take care of the rest. For further information on office relocations, <a href="<?=base_url()?>contact"><strong>contact us</strong></a> today.</p>
                  <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-2-4">
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/office-relocation.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/office-relocation-1.jpg" alt="Luxury Residential Building">
                      <hr>
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/office-relocation-2.jpg" alt="Luxury Residential Building">
                      <hr>
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/office-relocation-3.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <p><strong>PACKING :</strong> We have the right equipment and all of our staff trained to the highest levels in assisting with the packing of just about everything you will find in today's office or warehouse environment.<br>

                        <strong>MOVING :</strong> We utilise a range of modern trucks fitted with tail-lifts, tie rails and carpeted batons and air suspension to protect your furniture and effects whilst in our care.<br>

                        <strong>HANDYPERSON / INSTALLATIONS:</strong> Installers come equipped with full toolkits and fittings for handling just about every job in an office or warehouse.<br>

                        <strong>SETTLE IN :</strong> After the move it's critical for any organisation to have their staff resume normal work duties as quickly as possible. </p>
                      </div>
                    </div>
                    <br>
                  </div>
                </div>
                <hr>

              </div>
            </div>
          </div>
        </section>    
        <!-- /Top A -->

        <!-- Bottom -->
        <!-- 
        <section class="bottom">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-12">
                <div class="module title1">
                <h3 class="module-title">Prestigious offices we have moved so far</h3>
                  <div class="module-content">
                    <div class="uk-grid">
                      <div class="uk-width-medium-1-5">
                        <img src="<?=base_url()?>assets/default/images/elements/logo1.png" alt="Our Clients" class="uk-align-center">
                      </div>
                      <div class="uk-width-medium-1-5">
                        <img src="<?=base_url()?>assets/default/images/elements/logo2.png" alt="Our Clients" class="uk-align-center">
                      </div>
                      <div class="uk-width-medium-1-5">
                        <img src="<?=base_url()?>assets/default/images/elements/logo3.png" alt="Our Clients" class="uk-align-center">
                      </div>
                      <div class="uk-width-medium-1-5">
                        <img src="<?=base_url()?>assets/default/images/elements/logo4.png" alt="Our Clients" class="uk-align-center">
                      </div>
                      <div class="uk-width-medium-1-5">
                        <img src="<?=base_url()?>assets/default/images/elements/logo5.png" alt="Our Clients" class="uk-align-center">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>    
      -->
    <!-- /Bottom -->