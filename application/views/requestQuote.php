      <!-- Page Title -->
      <section class="page-title">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>Get A Quote</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
				<li><a class="pathway" href="<?=base_url()?>">Home</a></li>
				<li class="active">Get A Quote</li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Get A Quote -->
      <section class="main-body">
	    <div class="container">
		  <div class="row">
		    <div class="col-sm-12 col-md-12">
              <article itemtype="http://schema.org/Article" itemscope="" class="item item-page">
	            <meta content="en-GB" itemprop="inLanguage">
                <div itemprop="articleBody">
		          <div class="uk-grid">
                    <div class="uk-width-medium-1-2">
                      <p class="dropcap">1 <strong>A no-obligation quote for your requirement.</strong><br>Whether you want to shift your home, entire office or even a full fledged factory, we will produce a detailed quote outlining how we can meet your specific needs. </p>
                      <hr>
                      <p class="dropcap">2 <strong>A member of our team will be in touch with you promptly to arrange a meeting.</strong><br>Please fill out the following information as  accurately as possible, so we can send a quote tailored to your exact requirements.</p>
                      <hr>
                      <p class="dropcap">3 <strong>Dedicated Team with up-to-date Technology.</strong><br> We are committed to provide professional services to take care the needs of our customers. Moreover you can track your consignment and get on-line status displayed at your Fingertips. It’s progress 24 hours a day</p>
                      <hr>
                      
                      <img src="<?=base_url()?>assets/default/images/elements/tools.jpg" alt="Get A Quote">

                    </div>
                    <div class="uk-width-medium-1-2">
                      <div class="gbs3" id="gbs3">
					    <form action="staticpages/addQuote" enctype="multipart/form-data" method="post" name="quote" id="chronoform-quote" class="chronoform">
						  <div class="form-group" id="form-row-text1">
						    <label for="text1" class="control-label required_label">Your Name <i class="fa fa-asterisk" style="color:#ff0000; font-size:9px; vertical-align:top;"></i></label>
                            <div class="gcore-display-table">
							  <input name="clientName" id="text1" class="form-control" type="text" required>
							</div>
						  </div>
						  <div class="form-group" id="form-row-text2">
						    <label for="text2" class="control-label required_label">Email <i class="fa fa-asterisk" style="color:#ff0000; font-size:9px; vertical-align:top;"></i></label>
                            <div class="gcore-display-table">
							  <input name="clientEmail" id="text2" class="form-control" type="text" required>
							</div>
						  </div>
						  <div class="form-group" id="form-row-text3">
						    <label for="text3" class="control-label required_label">Phone <i class="fa fa-asterisk" style="color:#ff0000; font-size:9px; vertical-align:top;"></i></label>
                            <div class="gcore-display-table">
							  <input name="phoneNumber" id="text3" class="form-control" type="text" required>
							</div>
						  </div>
						  <div class="form-group" id="form-row-text3">
						    <label for="text3" class="control-label required_label">Company </i></label>
                            <div class="gcore-display-table">
							  <input name="clientCompany" id="text3" class="form-control" type="text" required placeholder="'Household' if not a company">
							</div>
						  </div>
						  <div class="form-group" id="form-row-text4">
						    <label for="text4" class="control-label required_label">Tentative Date Of Move<i class="fa fa-asterisk" style="color:#ff0000; font-size:9px; vertical-align:top;"></i></label>
                            <div class="gcore-display-table">
							  <input name="dateOfMove" id="text4" class="form-control" type="date" required>
							</div>
						  </div>
						  <div class="form-group" id="form-row-text5">
						    <label for="text5" class="control-label required_label">Zip Code </label>
                            <div class="gcore-display-table">
							  <input name="zipCode" id="text5" class="form-control" type="text" required>
							</div>
						  </div>

						  <div id="form-row-radio6" class="form-group">
						    <label class="control-label required_label" for="radio6">Type of project <i style="color:#ff0000; font-size:9px; vertical-align:top;" class="fa fa-asterisk"></i></label>
                            <div id="fin-radio6" class=" gcore-display-table">
							  <div id="fclmn">
							    <div id="fitem">
								  <label for="radio6" class="control-label">
								    <input type="radio" value="domesticRemoval" id="radio6" name="typeOfProject" checked>Domestic Removal
								  </label>
								</div>
                                <div id="fitem1">
								  <label for="radio61" class="control-label">
								    <input type="radio" value="internationalRemoval" id="radio61" name="typeOfProject">International Removal
								  </label>
								</div>
                                <div id="fitem2">
								  <label for="radio62" class="control-label">
								    <input type="radio" value="officeRemoval" id="radio62" name="typeOfProject">Office Removal
								  </label>
								</div>
                                
							  </div>
							</div>
						  </div>


                          <div class="form-group" id="form-row-text7">
						    <label for="text7" class="control-label required_label">Square footage </label>
                            <div class="gcore-display-table" id="fin-text7">
							  <input name="squareFootage" id="text7" class="form-control" type="text">
							</div>
						  </div>
						  <div class="form-group" id="form-row-textarea8">
						    <label for="textarea8" class="control-label required_label">Tell us more about yout project <i class="fa fa-asterisk" style="color:#ff0000; font-size:9px; vertical-align:top;"></i></label>
                            <div class="gcore-display-table" id="fin-textarea8">
							  <textarea name="description" id="textarea8" rows="10" cols="40" class="form-control" required=""></textarea>
							</div>
						  </div>
						  <div class="form-group" id="form-row-button9">
						    <div class="gcore-display-table" id="fin-button9">
							  <input name="button9" id="button9" value="Get a free quote now" class="btn btn-default" type="submit">
							</div>
						  </div>
						</form>
					  </div> 
                    </div>
                  </div> 	
 	            </div>
			  </article>
            </div>
		  </div>
	    </div>
	  </section>
	  <!-- /Get A Quote -->


<script>
$(document).ready(function()
{
	// process the form
    $('#chronoform-quote').submit(function(event) {

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '<?= base_url()."staticpages/addQuote";?>', // the url where we want to POST
            data        : $(this).serialize(), // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode         : true
        })
            // using the done promise callback
            .done(function(data) {
            	alert(Hello);
                // log data to the console so we can see
                console.log(data); 

                // here we will handle errors and validation messages
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
        $( "#chronoform-quote" ).fadeOut( "slow", function() {
    	$('#gbs3').html("<h3 align='center' style='color:green; padding-top:200px;'> <i class='fa fa-check-circle-o' aria-hidden='true'></i> We are in reciept of your Requirement<br> Shall get back to you soon!!").fadeIn();
  		});
    });

  
});
</script>
