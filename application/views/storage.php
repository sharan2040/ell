      <!-- Page Title -->
      <section class="page-title">
        <div class="container">   
          <div class="row">
            <div class="col-sm-12 col-md-12 title">
              <h2>Storage Services - Long Term &amp; Short Term</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
                <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
                <li><a class="pathway" href="#">Services</a></li>
                <li class="active">Storage Services</li>          
              </ol>
            </div>
          </div>
        </div>
      </section>
      <!-- /Page Title -->

      <!-- Top A -->
      <section class="main-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="module title3">
                <div class="module-content">
                  <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-2-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/storage-service.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/storage-service-1.jpg" alt="Luxury Residential Building">
                      <hr>
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/storage-service-2.jpg" alt="Luxury Residential Building">
                      <hr>
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/storage-service-3.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <p><strong>SECURE &amp; AFFORDABLE STORAGE OPTION :</strong> With storage services of ELL, your assets are monitored 24 hours a day. We have large warehouses in Dubai, Qatar, Abudabi, with secure facilities and individual storage compartments to meet your needs.</p>
                      <p> Our locations have 24-hour security and a modern system of water and fire protection to protect the stored objects. All units are equipped with dock leveler for easy loading and unloading of trucks and heavy or bulky objects. Whether for short or long periods, Elegant Line Logistics has the best solution for storing furniture, electronics, files and other objects, which ensures peace of mind for you and your family.<br>
                      </p>
                    </div>
                    <br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>    
        <!-- /Top A -->
