

  

  <!-- Bootstrap -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
  <script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

  <!-- AdminPlus -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

  <!-- App JS -->
  <script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

  <!-- Theme Colors -->
  <script src="<?=base_url()?>assets/admin/assets/js/colors.js"></script>

  <!-- Charts JS -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/raphael-min.js"></script>
  <script src="<?=base_url()?>assets/admin/assets/vendor/morris.min.js"></script>

  <!-- Initialize Charts -->

</body>

</html>