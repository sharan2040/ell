<!DOCTYPE html>
<html class="bootstrap-layout">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Dashboard</title>

  <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
  <meta name="robots" content="noindex">

  <script src="<?=base_url()?>assets/admin/assets/vendor/jquery.min.js"></script>

  <!-- Material Design Icons  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Roboto Web Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">

  <!-- App CSS -->
  <link type="text/css" href="<?=base_url()?>assets/admin/assets/css/style.min.css" rel="stylesheet">
  <link type="text/css" href="<?=base_url()?>assets/admin/assets/css/custom.css" rel="stylesheet">

  <!-- Charts CSS -->
  <link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/morris.min.css">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="icon" type="image/png" href="<?=base_url()?>assets/favicon.png" />

  

</head>

<body class="layout-container ls-top-navbar si-l3-md-up">

  <!-- Navbar -->
  <nav class="navbar navbar-light bg-white navbar-full navbar-fixed-top ls-left-sidebar">

    <!-- Sidebar toggle -->
    <button class="navbar-toggler pull-xs-left hidden-lg-up" type="button" data-toggle="sidebar" data-target="#sidebarLeft"><span class="material-icons">menu</span></button>

    <!-- Brand -->
    <a class="navbar-brand first-child-md" href="<?=base_url()?>admin/dashboard">Dashboard v1.0</a>

    <!-- Search -->
    <form class="form-inline pull-xs-left hidden-sm-down">
      <p style="font-size:12px; vertical-align:middle;"> |  Powered by <img src="<?=base_url()?>assets/default/images/codecub.png" style="display: inline; height: 12px;"> Codecub Softlabs</p>
     
    </form>
    <!-- // END Search -->

    <!-- Menu -->
    <ul class="nav navbar-nav pull-xs-right hidden-md-down">



      <!-- User dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link active dropdown-toggle p-a-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false">
          <img src="<?=base_url()?>assets/admin/assets/images/people/110/admin.jpg" alt="Avatar" class="img-circle" width="40">
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-list" aria-labelledby="Preview">
          <a class="dropdown-item" href="<?=base_url()?>admin/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
        </div>
      </li>
      <!-- // END User dropdown -->

    </ul>
    <!-- // END Menu -->

  </nav>
  <!-- // END Navbar -->

  <!-- Sidebar -->
  <div class="sidebar sidebar-left si-si-3 sidebar-visible-md-up sidebar-dark bg-primary" id="sidebarLeft" data-scrollable>

    <!-- Brand -->
    <a href="index.html" class="sidebar-brand" align="center">
      <img src="<?=base_url()?>assets/admin/assets/images/689.png" height="50" >
      <!-- <i class="material-icons">control_point</i> AdminPlus -->
    </a>

    <!-- User -->
    <a href="user-profile.html" class="sidebar-link sidebar-user">
      <img src="<?=base_url()?>assets/admin/assets/images/people/110/admin.jpg" alt="user" class="img-circle"> Administrator
    </a>
    <!-- // END User -->

    <!-- Menu -->
    <ul class="sidebar-menu sm-bordered sm-active-button-bg">
      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/dashboard">
          <i class="sidebar-menu-icon material-icons">home</i> Dashboard
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/enquiries">
          <i class="sidebar-menu-icon material-icons">help</i> Enquiries
        </a>
      </li>
      
     
<!-- 
       <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/dashboard">
          <i class="sidebar-menu-icon material-icons">mail</i> Send a Quote
        </a>
      </li>
       -->

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/notes">
          <i class="sidebar-menu-icon material-icons">note</i> Personal Notes
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/reminder">
          <i class="sidebar-menu-icon material-icons">notifications</i> Reminders
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/careers">
          <i class="sidebar-menu-icon material-icons">work</i> Careers
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/blog">
          <i class="sidebar-menu-icon material-icons">event</i> News & Events
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/projects">
          <i class="sidebar-menu-icon material-icons">timeline</i> Project Tracking
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/faq">
          <i class="sidebar-menu-icon material-icons">forum</i> FAQ
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/settings">
          <i class="sidebar-menu-icon material-icons">settings_applications</i> Settings
        </a>
      </li>
      

    </ul>
    <!-- // END Menu -->

    <!-- Activity -->
    <div class="sidebar-heading">Reminders for Today</div>
    <?php if(!empty($reminders)) :?>
    <ul class="sidebar-activity">
     
     <?php foreach($reminders as $reminder): ?>
      <li class="media">
        <div class="media-left">
          <div class="sidebar-activity-icon">
            <i class="material-icons">today</i>
          </div>
        </div>
        <div class="media-body">
          <?= $reminder->reminder?>
          <small>Added on <?= date('d M Y', strtotime($reminder->reminder_date))?></small>
        </div>
      </li>
    <?php endforeach;?>

    </ul>
  <?php else :?>
    <ul class="sidebar-activity">
    <li class="media">
        <div class="media-left">
          <div class="sidebar-activity-icon">
            <i class="material-icons">sentiment_dissatisfied</i>
          </div>
        </div>
        <div class="media-body">
          Hmm. I got nothing to remind you today
        </div>
      </li>
      </ul>
  <?php endif;?>
    <!-- // END Activity -->

    <!-- Stats -->
    <!-- <div class="sidebar-stats">
      <div class="sidebar-stats-lead text-primary">
        <span>97</span>
        <small class="text-success">
          <i class="material-icons md-middle">arrow_upward</i>
          <span class="icon-text">3.4%</span>
        </small>
      </div>
      <small>TOTAL ENQUIRIES</small>
    </div> -->
    <!-- // END Stats -->

  </div>
  <!-- // END Sidebar -->

  <!-- Right Sidebars -->

 