	 <!-- Bottom B -->
	 <section class="bottom-b">
	 	<div class="container">
	 		<div class="row">
	 			<div class="col-sm-4 col-md-4">
	 				<div class="module title3">
	 					<h3 class="module-title">News &amp; Events</h3>
	 					<div class="module-content">
	 						<dl class="uk-description-list-line">
	 							<?php if(!empty($recentBlogs)):?>
	 								<?php foreach($recentBlogs as $blog):?>
	 									<dt>
	 										<a href="<?= base_url().'blog/'.$blog->slug;?>"><strong><?= $blog->blog_title?></strong></a> 
	 										<span title="<?= $blog->views?> user views" data-uk-tooltip="" class="uk-badge uk-badge-percent uk-float-right"><?= $blog->views?></span>
	 									</dt>
	 									<dd><?= substr(strip_tags($blog->contents), 0, 105);?>...</dd>
	 								<?php endforeach; ?>
	 							<?php else :?>
	 								<dt> <i class="fa fa-comments" aria-hidden="true"></i> There are no news currently!</dt>
	 							<?php endif; ?>
	 						</dl>
	 					</div>
	 				</div>
	 			</div>
	 			<div class="col-sm-5 col-md-5">
	 				<div class="module title3">
	 					<h3 class="module-title">About us</h3>
	 					<div class="module-content">
	 						<p align="justify"><strong>Elegant Line Logistics LLC</strong> is a UAE based Logistics &amp; shipping company with more than <strong>6 years experience</strong> in the field. At ELL we believe in fostering fruitful, long term relationships that enable our clients to extend their business across borders in the most reliable and efficient way possible.</p>
	 						<p>
	 							<div class="row">
	 								<div class="col-sm-4 col-md-4">
	 									<a href="<?= base_url()?>assets/default/images/movers_poe.jpg" data-uk-lightbox="{group:'group1'}" title="Membership with : Movers P.O.E | Empowering your Business through Technology">
	 										<img src="<?= base_url()?>assets/default/images/elements/poe.jpg" />
	 									</a>
	 								</div>
	 								<div class="col-sm-4 col-md-4">
	 									<a href="<?= base_url()?>assets/default/images/iam.jpg" data-uk-lightbox="{group:'group1'}" title="Membership with : International Association of Movers | Moving Forward, Together">
	 										<img src="<?= base_url()?>assets/default/images/elements/iam.jpg" />
	 									</a>
	 								</div>
	 								<div class="col-sm-4 col-md-4">
	 									<a href="<?= base_url()?>assets/default/images/imc_world.jpg" data-uk-lightbox="{group:'group1'}" title="Membership with : International Mobility Convention | You're in Good Company">
	 										<img src="<?= base_url()?>assets/default/images/elements/imc.jpg" />
	 									</a>
	 								</div>
	 								<!-- <div class="col-sm-4 col-md-4"><img src="<?= base_url()?>assets/default/images/elements/iam.jpg" /></div>
	 								<div class="col-sm-4 col-md-4"><img src="<?= base_url()?>assets/default/images/elements/imc.jpg" /></div> -->
	 							</div>


	 						</p>
                  <!-- <p>
				    <a href="<?= $settings[0]->facebook_link ?>" target="_blank" class="uk-icon-button uk-icon-facebook"></a>
                    <a href="<?= $settings[0]->twitter_link ?>" class="uk-icon-button uk-icon-twitter"></a>
                    <a href="<?= $settings[0]->google_link ?>" class="uk-icon-button uk-icon-google"></a>
                    <a href="<?= $settings[0]->linkedin_link ?>" class="uk-icon-button uk-icon-linkedin"></a>
                </p> -->
            </div>
        </div>
    </div>


    <div class="col-sm-3 col-md-3">
    	<div class="module title3">
    		<h3 class="module-title">Contact us</h3>
    		<div class="module-content">
    			<p><i class="uk-icon-envelope"></i>&nbsp;&nbsp;Email us at: <?= $settings[0]->contact_email ?></p>
    			<p><i class="uk-icon-phone"></i>&nbsp;&nbsp;<strong style="font-size: 20px;"> <?= $settings[0]->contact_phone ?></strong></p>
    			<p><i class="uk-icon-print"></i>&nbsp;&nbsp;<strong style="font-size: 20px;"> +555.123.4568</strong></p>
    			<p><i class="uk-icon-building-o"></i>&nbsp;&nbsp; <?= $settings[0]->company_address ?>
    			</div>
    		</div>
    	</div>





    </div>
</div>
</section>		  
<!-- /Bottom B -->

<!-- Footer Top -->
<section class="footer-top">
</section>	  
<!-- /Footer Top -->	  

<!-- Footer -->
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-6" id="footer1">
				<span class="copyright"> &copy; <?php echo date('Y');?> Elegant Line Logistics. All Rights Reserved | Follow us : 
					<a href="<?= $settings[0]->facebook_link ?>" target="_blank" class="uk-icon-button uk-icon-facebook"></a>
					<a href="<?= $settings[0]->twitter_link ?>" class="uk-icon-button uk-icon-twitter"></a>
					<a href="<?= $settings[0]->google_link ?>" class="uk-icon-button uk-icon-google"></a>
					<a href="<?= $settings[0]->linkedin_link ?>" class="uk-icon-button uk-icon-linkedin"></a>
				</span>
			</div>
			
			<div class="col-sm-1 col-md-1 footer2" id="footer2">
				<div class="module ">
					<div class="module-content">
						<a href="#" class="uk-icon-button uk-icon-chevron-up" data-uk-smooth-scroll=""></a>
					</div>
				</div>
			</div>
			<div class="col-sm-5 col-md-5" id="footer13" align="right" style="line-height: 25px;">
				<span class="copyright"><a href="<?php echo base_url();?>terms-and-conditions">Terms &amp; Conditions</a> | Powered by <a href="http://www.codecub.in/" target="_blank"><img src="<?=base_url()?>assets/default/images/codecub.png" style="display: inline; height: 15px;"> Codecub Softlabs</a>
				</span>
			</div>
		</div>
	</div>
</footer>	  
<!-- Footer -->

<!-- Off Canvas Menu -->
<div class="offcanvas-menu">
	<a class="close-offcanvas" href="#"><i class="uk-icon-remove"></i></a>
	<div class="offcanvas-inner">
		<div class="module">
			<h3 class="module-title">Menu</h3>
			<div class="module-content">
				<ul class="nav menu">
					<li><a href="<?php echo base_url();?>">Home</a></li>
					<li class="current active deeper parent">
						<a>About</a>
						<ul style="display: none;" class="nav-child unstyled small">
							<li><a href="<?php echo base_url();?>about-us">Company Profile</a></li>
							<li><a href="<?php echo base_url();?>careers">Careers</a></li>
							<li><a href="<?php echo base_url();?>faq">FAQ's</a></li>
						</ul>
						<span class="toggle-icon fa fa-angle-down"></span>				  
					</li>	
					<li class="current active deeper parent">
						<a>Services</a>
						<ul style="display: none;" class="nav-child unstyled small">
							<li><a href="<?php echo base_url();?>local-removal">Local Removal</a></li>
							<li><a href="<?php echo base_url();?>international-removal">International Removal</a></li>
							<li><a href="<?php echo base_url();?>office-removal">Office Removal</a></li>
							<li><a href="<?php echo base_url();?>vehicle-relocation">Vehicle Relocation</a></li>
							<li><a href="<?php echo base_url();?>industrial-packing">Industrial Packing</a></li>
							<li><a href="<?php echo base_url();?>cargo-lashing-services">Cargo Lashing</a></li>
							<li><a href="<?php echo base_url();?>freight-forwarding">Freight Forwarding</a></li>
							<li><a href="<?php echo base_url();?>storage">Storage Facilities</a></li>
						</ul>
						<span class="toggle-icon fa fa-angle-down"></span>				  
					</li>				
					<li><a href="<?php echo base_url();?>track-your-shipment">Track your Shipment</a></li>
					<li><a href="<?php echo base_url();?>news">News &amp; Events</a></li>
					<li><a href="<?php echo base_url();?>contact">Contact Us</a></li>
					<li><a href="<?php echo base_url();?>get-a-quote">Get Free Quote!</a></li>

				</ul>
			</div>
		</div>
	</div>
</div>	  
<!-- /Off Canvas Menu -->
</div>


<!-- Scripts placed at the end of the document so the pages load faster -->


<!-- Uikit scripts -->
<script src="<?=base_url()?>assets/default/assets/js/uikit.min.js"></script>	
<script src="<?=base_url()?>assets/default/assets/js/slideshow.min.js"></script>  
<script src="<?=base_url()?>assets/default/assets/js/slideset.min.js"></script> 	
<script src="<?=base_url()?>assets/default/assets/js/sticky.min.js"></script>
<script src="<?=base_url()?>assets/default/assets/js/tooltip.min.js"></script>	
<script src="<?=base_url()?>assets/default/assets/js/parallax.min.js"></script>
<script src="<?=base_url()?>assets/default/assets/js/lightbox.min.js"></script>
<script src="<?=base_url()?>assets/default/assets/js/grid.min.js"></script>

<!-- WOW scripts -->
<script src="<?=base_url()?>assets/default/assets/js/wow.min.js"></script>
<script> new WOW().init(); </script>

<!-- Оffcanvas Мenu scripts -->
<script src="<?=base_url()?>assets/default/assets/js/offcanvas-menu.js"></script> 	

<!-- Template scripts -->
<script src="<?=base_url()?>assets/default/assets/js/template.js"></script> 	

<!-- Bootstrap core JavaScript -->
<script src="<?=base_url()?>assets/default/bootstrap/js/bootstrap.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?=base_url()?>assets/default/assets/js/ie10-viewport-bug-workaround.js"></script>


</body>
</html>
