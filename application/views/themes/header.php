<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Meta -->
    <meta name="description" content="Hammer construction & renovation responsive html template, can be used for any type of business or corporate site">
	<meta name="keywords" content="hammer, construction, renovation, design, planning, boostrap, responsive, html5, css3, jquery, theme, uikit, multicolor, parallax" />
    <meta name="author" content="dhsign">
	<meta name="robots" content="index, follow" />
	<meta name="revisit-after" content="3 days" />

 	<link rel="icon" type="image/png" href="<?=base_url()?>assets/favicon.png" />

	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>	
    <title><?= $title?> | Elegant Line Logistics</title>

        <!-- Jquery scripts -->
    <script src="<?=base_url()?>assets/default/assets/js/jquery.min.js"></script>

	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="<?=base_url()?>assets/default/assets/css/uikit.min.css" rel="stylesheet">	
    <link href="<?=base_url()?>assets/default/assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/default/assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/default/assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/default/assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/default/bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="<?=base_url()?>assets/default/assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket Strips CSS -->
	<link href="<?=base_url()?>assets/default/assets/css/product.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/default/assets/css/strips.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/default/assets/css/quotes.css" rel="stylesheet" />
    <!-- Font Awesome -->	
    <link href="<?=base_url()?>assets/default/assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="<?=base_url()?>assets/default/assets/css/helper.css" rel="stylesheet">	
    <link href="<?=base_url()?>assets/default/assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="<?=base_url()?>assets/default/assets/css/template.css" rel="stylesheet">	
    <link href="<?=base_url()?>assets/default/assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
	  <!-- Top Bar -->	
      <section class="hidden-xs top-bar" id="top-bar">
	    <div class="container">
          <div class="row">
		    <div class="col-sm-6 col-md-6" id="top2">
			  <ul class="contact-info">
			    <li>
				  <i class="uk-icon-phone"></i> <?= $settings[0]->contact_phone ?>
				</li>
				<li>
				  <i class="uk-icon-envelope"></i> <a href="<?= $settings[0]->contact_email ?>"> <?= $settings[0]->contact_email ?></a>
				</li>
			  </ul>
			</div>
		    <div class="col-sm-6 col-md-6 top1">
		    <div class="pull-right" style="font-size:13px;"> Working Hours : <?= $settings[0]->working_hours ?>| <img src="<?=base_url()?>assets/default/images/uae.gif" style="display: inline; height: 13px;"> United Arab Emirates </div>
			  <!-- <ul class="social-icons">
			    <li>
				  <a href="https://www.facebook.com/elegantlinelogistics/" target="_blank"><i class="uk-icon-facebook"></i></a>
				</li>
				<li>
				  <a href="https://twitter.com/" target="_blank"><i class="uk-icon-twitter"></i></a>
				</li>
				<li>
				  <a href="https://plus.google.com/" target="_blank"><i class="uk-icon-google-plus"></i></a>
				</li>
				<li>
				  <a href="https://www.pinterest.com/" target="_blank"><i class="uk-icon-pinterest"></i></a>
				</li>
				<li>
				  <a href="https://www.youtube.com/" target="_blank"><i class="uk-icon-youtube"></i></a>
				</li>
			  </ul> -->
			</div>
		  </div>
        </div>
	  </section>
	  <!-- /Top Bar -->

	  <!-- Sticky Menu -->	  
      <div id="header-sticky-wrapper" class="sticky-wrapper" style="height: 86px;">
	    <header data-uk-sticky id="header" class="header">
		  <div class="container">
		    <div class="row" style="position: relative;">
			  <div class="col-xs-8 col-sm-2 col-md-2" id="logo">
				<a href="<?=base_url()?>" class="logo">
				  <h1>
				    <img alt="Hammer" src="<?=base_url()?>assets/default/images/presets/preset1/logo.png" class="default-logo">
                    <img width="190" height="35" alt="Hammer" src="<?=base_url()?>assets/default/images/presets/preset1/logo2x.png" class="retina-logo">					
				  </h1>
				</a>
			  </div>
			  <div class="col-xs-4 col-sm-10 col-md-10" id="menu">
			    <div><a href="#" id="offcanvas-toggler"><i class="uk-icon-bars"></i></a></div>
                <div>
				  <ul class="megamenu-parent menu-zoom hidden-xs">
					
                    <li class="menu-item"><a href="<?=base_url()?>">Home</a></li>
					<li class="menu-item has-child current-item">
					  <a href="#">About</a>
					  <div class="dropdown dropdown-main menu-right width200">
					    <div class="dropdown-inner">
						  <ul class="dropdown-items">
						    <li class="menu-item"><a href="<?=base_url()?>about-us">Company Profile</a></li>
						    <li class="menu-item"><a href="<?=base_url()?>careers">Careers</a></li>
						    <li class="menu-item"><a href="<?=base_url()?>faq">FAQ's</a></li>
							<!-- <li class="menu-item"><a href="news">News &amp; Events</a></li> -->
						  </ul>
						</div>
					  </div>
					</li>
                    <li class="menu-item has-child menu-justify">
                      <a href="#">Services</a>
					  <div class="dropdown dropdown-main dropdown-mega menu-full container700">
					    <div class="dropdown-inner">
						  <div class="row">
                            <div class="col-sm-4 width300">
							  <img src="<?php echo base_url();?>assets/default/images/menu.jpg" style="padding: 20px;">
							</div>
                            <div class="col-sm-4 width200">
							  <ul class="mega-group">
							    <li class="menu-item1 has-child">
								  <ul class="mega-group-child dropdown-items">
                                    <li class="menu-item"><a href="<?php echo base_url();?>local-removal">Local Removal</a></li>
                                    <li class="menu-item"><a href="<?php echo base_url();?>international-removal">International Removal</a></li>
                                    <li class="menu-item"><a href="<?php echo base_url();?>office-removal">Office Removal</a></li>
                                    <li class="menu-item"><a href="<?php echo base_url();?>vehicle-relocation">Vehicle Relocation</a></li>
                                  </ul>
                                </li>
						      </ul>
							</div>
                            <div class="col-sm-4 width200">
							  <ul class="mega-group">
							    <li class="menu-item1 has-child">
								  <ul class="mega-group-child dropdown-items">
                                    <li class="menu-item"><a href="<?php echo base_url();?>industrial-packing">Industrial Packing</a></li>
                                    <li class="menu-item"><a href="<?php echo base_url();?>cargo-lashing-services">Cargo Lashing </a></li>
                                    <li class="menu-item"><a href="<?php echo base_url();?>freight-forwarding">Freight Forwarding</a></li>
                                    <li class="menu-item"><a href="<?php echo base_url();?>storage">Storage Facilities</a></li>
                                  </ul>
                                </li>
						      </ul>
							</div>							
                          </div>
						</div>
					  </div>
                    </li>
                    <li class="menu-item"><a href="<?=base_url()?>track-your-shipment">Track your Shipment</a></li>
                    <li class="menu-item"><a href="<?=base_url()?>news">News &amp; Events</a></li>
                    <li class="menu-item"><a href="<?=base_url()?>contact">Contact Us</a></li>
                    <li class="menu-item has-child pulse"><a href="<?=base_url()?>get-a-quote" style="color:#FFB400;"><i class="fa fa-info-circle" aria-hidden="true"></i> Get a Quote</a></li>
                   			
				  </ul>
				</div>				
			  </div>
			</div>
		  </div>
		</header>
	  </div>
	  <!-- /Sticky Menu -->	