<!-- Page Title -->
<section class="page-title">
  <div class="container">   
    <div class="row">
      <div class="col-sm-12 col-md-12 title">
        <h2>Know your order status</h2>
        <ol class="breadcrumb">
          <li>You are here: &nbsp;</li>
          <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
          <li class="active">Order Status</li>          
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- /Page Title -->

<!-- Contact Us -->
<section class="main-body">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <p><strong>ELL’s online tracking</strong> allows you to access tracking information and confirm the delivery of your item by using the unique tracking ID assigned to you. You can find the <ins> Tracking ID </ins> on the Receipt handed over to you at the office Counter at the time of booking. The tracking system is updated periodically to provide you with the most current information available about the location and status of your consignment.</p>
        <hr>
      </div>

      <div class="col-md-8 col-md-offset-2">
        <br>
        <form class="form-horizontal">
          <fieldset>
            <div class="form-group">
              <label for="inputName" class="control-label label-left col-xs-3">Your Unique tracking ID * </label>
              <div class="col-xs-9">
                <input type="text" class="form-control" id="uniqueId" placeholder=" Enter your unique ID" name="uniqueId" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label label-left col-xs-3"></label>			
              <div class="col-xs-9">
                <button class="btn btn-success validate" id="getStatusButton" data-toggle="modal" data-target="#myModal" type="button" aria-invalid="false">Get Status</button>
              </div>
            </div>
          </fieldset>				
        </form>	


      </div>
      <div class="col-md-12">
        <hr>
      </div>



      <div class="col-md-6 col-md-offset-4 hidden">
        <div class="module title1">
          <div class="module-content">
            <div class="uk-block uk-block-primary uk-contrast">
              <div class="uk-container">
                <div class="uk-grid uk-grid-match" data-uk-grid-margin="">
                  <div class="uk-width-medium-1-1  uk-flex uk-flex-middle">
                    <div class="uk-panel">
                      <ul>
                        <li>Hello</li>
                        <li>Sharan</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  </section>
  <!-- /Contact Us -->




  <!-- Modal -->

  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#0E2947; color:#FFF;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Consignment/Shipment status for Unique ID : <font id="modalTitle"></font></h4>
        </div>
        <div class="modal-body">
          <p id="modalBody"></p>
        </div>
        <div class="modal-footer" style="border-bottom: medium dashed #0E2947;">
          <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <script>
    $(document).ready(function()
    {
      $("#getStatusButton").click(function(){
        var uniqueId = $('#uniqueId').val();
    // alert(uniqueId);
    $.ajax({
      url: "<?= base_url().'fetch-details'?>",
      type: 'POST',
      data: uniqueId,
      success: function(result) {

        $('#modalTitle').html(uniqueId);
        $('#modalBody').html(result);
              // alert(result);
            }
          });
  });

    });
  </script>