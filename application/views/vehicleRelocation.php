      <!-- Page Title -->
      <section class="page-title">
        <div class="container">   
          <div class="row">
            <div class="col-sm-12 col-md-12 title">
              <h2>VEHICLE RELOCATION SERVICES</h2>
              <ol class="breadcrumb">
                <li>You are here: &nbsp;</li>
                <li><a class="pathway" href="<?php echo base_url();?>">Home</a></li>
                <li><a class="pathway" href="<?php echo base_url();?>">Services</a></li>
                <li class="active">Vehicle Relocation</li>          
              </ol>
            </div>
          </div>
        </div>
      </section>
      <!-- /Page Title -->

      <!-- Top A -->
      <section class="main-body">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="module title3">
                <div class="module-content">
                  <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-2-4">
                        <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/automobile-relocation.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/automobile-relocation-1.jpg" alt="Luxury Residential Building">
                      <hr>
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/automobile-relocation-3.jpg" alt="Luxury Residential Building">
                      <hr>
                      <img width="1150" height="500" src="<?=base_url()?>assets/default/images/services/automobile-relocation-2.jpg" alt="Luxury Residential Building">
                    </div>
                    <div class="uk-width-medium-1-4">
                      <p><strong>ELL</strong> is dedicated to providing  professional door-to-door vehicle relocation services,safe and secure with vehicles blocked, braced and tied down to ensure absolute security during relocation. By continuously reviewing best practices, we are focused on maximizing efficiencies and increasing overall customer satisfaction.</p>

                      <p> We continually look for ways to make our relocation services better. Your time, your budget and your belongings are important factors in planning your move. With ELL, you’ll know where your belongings are at all phases of the move and when you can expect them to arrive. <a href="<?=base_url()?>contact"><strong>Contact us</strong></a> now to discuss how we can help with your vehicle relocation service requirements.</p>
                    </div>
                  </div>
                </div>
                <hr>
              </div>
            </div>
          </div>
        </div>
      </section>    
      <!-- /Top A -->
