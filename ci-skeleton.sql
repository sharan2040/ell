-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 01, 2016 at 06:26 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ci-skeleton`
--

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE IF NOT EXISTS `enquiries` (
  `enquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `origin` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `dimension` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `total_weight` int(11) NOT NULL,
  `extra_comments` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 - New/Unread, 1 - Attended, 2- Rejected',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`enquiry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1002 ;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`enquiry_id`, `full_name`, `company`, `phone`, `email`, `origin`, `destination`, `dimension`, `description`, `total_weight`, `extra_comments`, `status`, `created_date`) VALUES
(2, 'Sharan', 'Cement Industry', '9895962040', 'sharan2040@gmail.com', 'Mumbai', 'Dubai', '45x65x48', 'None', 185, 'dffdfdbdfb', 1, '2016-03-30 13:04:40'),
(3, 'Kiran', 'Meat Packers LLC', '9895962040', 'kiran@gmail.com', 'Kochi', 'Mumbai', '21x65x45', 'Meat is a good source of protein, vitamins and minerals in your diet. However, the Department of Health has advised that people who eat a lot of red and processed meat a day (more than 90g cooked weight) cut down to 70g. Making healthier choices can help you eat meat as part of a healthy, balanced diet.', 345, 'dffdfdbdfb', 1, '2016-03-30 13:04:40'),
(1000, 'Gokul T', 'Milk Express LLC', '7856412365', 'gokul@gmail.com', 'Kochi', 'Dubai', '75x98x64', 'When a single genetic mutation first let ancient Europeans drink milk, it set the stage for a continental upheaval. Milk is a pale liquid produced by the mammary glands of mammals. It is the primary source of nutrition for infant mammals before they are able to digest othe', 7500, '', 2, '2016-04-01 09:56:10'),
(1001, 'Ajin', 'Doubts Cargo LLC', '456983210', 'test@gmail.com', 'Manglore', 'South Africa', '75x69x12', 'Doubts Corp', 250, '', 0, '2016-04-01 11:17:14');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `note_content` text NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`note_id`, `note_content`, `created_on`) VALUES
(15, 'sdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2016-03-30 12:12:58'),
(18, 'sdfsdf\r\ndfgdfg\r\n\r\n\r\ndfgdfgdfg\r\n\r\nfghfg', '2016-03-30 12:16:41'),
(20, 'The following example will show you how to call a function after \r\nwaiting for some time. The example uses the jQuery delay() \r\nmethod to set the time interval for', '2016-03-30 12:32:38'),
(21, 'sdfdsf\r\n\r\nsdfsfdsfsdf\r\n\r\n\r\nsdfsdfsdfsdf', '2016-03-31 17:49:29');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE IF NOT EXISTS `quotes` (
  `quote_id` int(11) NOT NULL AUTO_INCREMENT,
  `enquiry_id` int(11) NOT NULL,
  `quote_amount` int(11) NOT NULL,
  `mail_body` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`quote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`quote_id`, `enquiry_id`, `quote_amount`, `mail_body`, `created_date`) VALUES
(2, 1001, 2500, 'Dear Sir\r\n\r\nSubject: Title of E-mail in Initial Capitals\r\n\r\nEngineers and scientists use e-mails to make requests, to answer questions, and to give announcements. E-mails are read quickly. For that reason, get to the point in the first paragraph--the first sentence, if possible. In other words, state what you want up front. Be careful about e-mails that make complaints, which are usually better handled in person.\r\n\r\nIn e-mails, keep the sentence lengths and paragraph lengths relatively short. Sentences should average fewer than twenty words, and paragraphs should average fewer than seven lines. In the format suggested here, you should single space your e-mails, skip a line between paragraphs, and use a typeface that is easily read on a computer. If possible, keep the total e-mail length to a length that can be viewed entirely on the screen.\r\n\r\nBecause the reader sees only the title of your e-mail in the Inbox or in the folder where it has been filed, give some thought to that title. Choose a title that orients the reader to the subject of the e-mail and, if possible, distinguishes your e-mail from other e-mails about that subject. For example, choose "Proposal Draft for Our ME 440W Design Project" as opposed to "Design Project" or "ME 440W."\r\n\r\nWith e-mails, send copies to anyone whose name you mention in the e-mail or who would be directly affected by the e-mail. Also, be sure to mention explicitly any attachments. Finally, remember that final paragraphs of e-mails generally tell readers what you want them to do or what you will do for them.\r\n\r\nSincerely, \r\n\r\nYour Name \r\nYour Contact Information', '2016-04-01 12:53:47');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`settings_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `type`, `value`) VALUES
(1, 'company_email', 'test@company.com'),
(2, 'company_phone', '12345678910');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `full_name`, `password_hash`, `status`, `created_at`, `updated_at`) VALUES
(1, 'sharan2040@gmail.com', 'Sharan Mohandas', 'password', 1, '2016-03-14 04:15:15', '2016-03-15 13:23:46');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
